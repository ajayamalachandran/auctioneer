import 'dart:io';

import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/core/flutter-toast.dart';
import 'package:auctioneer/core/loader.dart';
import 'package:auctioneer/core/tools/Image_picker_methods.dart';
import 'package:auctioneer/custom.dart';
import 'package:auctioneer/hooks/use-navbar.dart';
import 'package:auctioneer/routes.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/widgets/text_input.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';

class NewAution extends HookWidget {
  final kind;
  const NewAution({Key key, this.kind = 'normal_auction'}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final currentDateTime = DateTime.now();

    final dropdownCategoryValue = useValueNotifier({'id': 1, 'name': 'Copper'});
    final dropdownSubcategoryValue = useValueNotifier(
        {'id': 1, 'name': 'Class A', 'name_with_category': 'Copper - Class A'});
    final radioNotifier =
        useValueNotifier(kind == 'normal_auction' ? 'sell' : 'buy');
    final radioTypeNotifier = useValueNotifier(kind);

    final formLoader = useValueNotifier(false);

    final categoryNotifier = useValueNotifier([]);
    final varientNotifier = useValueNotifier([]);
    final varientStaticNotifier = useValueNotifier([]);
    final volumeController = useTextEditingController();
    final auctionEndTimeController = useTextEditingController();
    final descriptionController = useTextEditingController();
    final productImageController = useTextEditingController();
    final unitPriceController = useValueNotifier(
        MoneyMaskedTextController(thousandSeparator: ',', precision: 0));

    bool isScreenWide = MediaQuery.of(context).size.width >= 740;

    final navbarHook = useNavbar();

    radioOnChange(value) {
      // radioNotifier.value = value;
    }

    radioTypeOnChange(value) {
      // radioTypeNotifier.value = value;
    }

    Future<Null> selectDate(BuildContext context) async {
      final DateTime pickedDate = await showDatePicker(
          context: context,
          initialDate: DateTime.now(),
          initialDatePickerMode: DatePickerMode.day,
          firstDate: DateTime(2015),
          lastDate: DateTime(2101));
      if (pickedDate != null) {
        final TimeOfDay pickedTime = await showTimePicker(
          context: context,
          initialTime: TimeOfDay.now(),
        );
        if (pickedTime != null) {
          auctionEndTimeController.text = DateTime(
                  pickedDate.year,
                  pickedDate.month,
                  pickedDate.day,
                  pickedTime.hour,
                  pickedTime.minute)
              .toString();
        }
      }
    }

    uploadImageToS3(File image) async {
      formLoader.value = true;

      Directory tempDir = await getTemporaryDirectory();
      String tempPath = tempDir.path;
      File compressedImage =
          await imageCompression(image, tempPath + 'compressed.jpg');
      final url = await getSignedUrl(compressedImage.path);
      if (url['signedUrl'] != null) {
        final img = await uploadImage(url['signedUrl'], compressedImage);
        productImageController.text = img;
        formLoader.value = false;
      } else {
        formLoader.value = false;
      }
    }

    getCategoriesAndVarients() async {
      var variantResponse = await getVarients();
      var categoriesResponse = await getCategories();
      dropdownCategoryValue.value = categoriesResponse[0];
      dropdownSubcategoryValue.value = variantResponse[0];
      categoryNotifier.value = categoriesResponse;
      varientStaticNotifier.value = variantResponse;
      varientNotifier.value = variantResponse;
    }

    useEffect(() {
      getCategoriesAndVarients();
    }, []);

    bool validate() {
      return radioNotifier.value != null &&
          dropdownCategoryValue.value != null &&
          dropdownSubcategoryValue.value != null &&
          volumeController.value.text.length > 0 &&
          auctionEndTimeController.value.text.length > 0 &&
          unitPriceController.value.numberValue > 0;
    }

    useListenable(radioNotifier);
    useListenable(categoryNotifier);
    useListenable(varientNotifier);
    useListenable(varientStaticNotifier);
    useListenable(radioTypeNotifier);
    useListenable(dropdownCategoryValue);
    useListenable(dropdownSubcategoryValue);
    useListenable(formLoader);
    useListenable(volumeController);
    useListenable(auctionEndTimeController);
    useListenable(unitPriceController.value);

    return categoryNotifier.value.length > 0 &&
            varientNotifier.value.length > 0 &&
            !formLoader.value
        ? Scaffold(
            backgroundColor: Colors.grey[100].withOpacity(0.6),
            drawer: MediaQuery.of(context).size.width > 800
                ? null
                : navbarHook.drawer,
            appBar: navbarHook.appBar,
            body: SingleChildScrollView(
              child: Form(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                child: Center(
                  child: Padding(
                    padding: isScreenWide
                        ? EdgeInsets.symmetric(vertical: 30)
                        : EdgeInsets.symmetric(vertical: 0),
                    child: Container(
                      width: 700,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 2,
                              blurRadius: 2,
                              offset: Offset(0.0, 0.75)),
                        ],
                      ),
                      child: Column(children: [
                        SizedBox(height: 20),
                        '경매'.styled(profileSideHeading(context)),
                        SizedBox(height: 20),
                        profileContainer(
                          children: [
                            SizedBox(height: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                '카테고리'.styled(inputHint),
                                SizedBox(height: 8),
                                Container(
                                  width: 400,
                                  child: DropdownButtonFormField(
                                    value: dropdownCategoryValue.value,
                                    icon: const Icon(Icons.arrow_drop_down),
                                    iconSize: 24,
                                    elevation: 16,
                                    style: TextStyle(
                                        color:
                                            Color(0xff1f2a7c).withOpacity(0.4)),
                                    onChanged: (newValue) {
                                      varientNotifier.value =
                                          varientStaticNotifier.value;
                                      dropdownCategoryValue.value = newValue;
                                      varientNotifier.value = varientNotifier
                                          .value
                                          .where((varient) =>
                                              varient['category']['id'] ==
                                              newValue['id'])
                                          .toList();
                                      dropdownSubcategoryValue.value =
                                          varientNotifier.value[0];
                                    },
                                    decoration: InputDecoration(
                                        filled: true,
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.grey, width: 2),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        hintStyle:
                                            TextStyle(color: Colors.black),
                                        fillColor: Colors.grey[100]),
                                    items: categoryNotifier.value
                                        .map<DropdownMenuItem<dynamic>>(
                                            (value) {
                                      return DropdownMenuItem<dynamic>(
                                        value: value,
                                        child: Text(
                                          value['name'],
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 10),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                '서브 카테고리'.styled(inputHint),
                                SizedBox(height: 8),
                                Container(
                                  width: 400,
                                  child: DropdownButtonFormField(
                                    value: dropdownSubcategoryValue.value,
                                    icon: const Icon(Icons.arrow_drop_down),
                                    iconSize: 24,
                                    elevation: 16,
                                    style: TextStyle(
                                        color:
                                            Color(0xff1f2a7c).withOpacity(0.4)),
                                    onChanged: (newValue) {
                                      dropdownSubcategoryValue.value = newValue;
                                    },
                                    decoration: InputDecoration(
                                        filled: true,
                                        enabledBorder: OutlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Colors.grey, width: 2),
                                          borderRadius: BorderRadius.all(
                                            Radius.circular(10),
                                          ),
                                        ),
                                        hintStyle:
                                            TextStyle(color: Colors.black),
                                        fillColor: Colors.grey[100]),
                                    items: varientNotifier.value
                                        .map<DropdownMenuItem<dynamic>>(
                                            (value) {
                                      return DropdownMenuItem<dynamic>(
                                        value: value,
                                        child: Text(value['name'],
                                            style:
                                                TextStyle(color: Colors.black)),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(height: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                '경매 유형'.styled(inputHint),
                                SizedBox(height: 5),
                                Wrap(
                                  alignment: WrapAlignment.start,
                                  direction: Axis.horizontal,
                                  crossAxisAlignment: WrapCrossAlignment.center,
                                  runAlignment: WrapAlignment.center,
                                  children: <Widget>[
                                    Radio(
                                      toggleable: false,
                                      activeColor: Colors.orange[700],
                                      value: 'normal_auction',
                                      groupValue: radioTypeNotifier.value,
                                      onChanged: radioTypeOnChange,
                                    ),
                                    '경매'.styled(inputHint),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Radio(
                                      toggleable: false,
                                      activeColor: Colors.orange[700],
                                      value: 'reverse_auction',
                                      groupValue: radioTypeNotifier.value,
                                      onChanged: radioTypeOnChange,
                                    ),
                                    '역경매'.styled(inputHint),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(height: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                '물량'.styled(inputHint),
                                SizedBox(height: 8),
                                TextInputWithPrefixSuffixMaterial(
                                    inputType: TextInputType.number,
                                    backgroundColor: Colors.grey[100],
                                    borderRadius: 10,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                    hint: '',
                                    controller: volumeController,
                                    validator: (arg) =>
                                        arg.length == 0 ? '음량 필수' : null),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                '단가 (톤)'.styled(inputHint),
                                SizedBox(height: 8),
                                TextInputWithPrefixSuffixMaterial(
                                    inputType: TextInputType.number,
                                    backgroundColor: Colors.grey[100],
                                    borderRadius: 10,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                    hint: '',
                                    controller: unitPriceController.value,
                                    validator: (arg) =>
                                        arg.length == 0 ? '단가 필수' : null),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                kind == "normal_auction"
                                    ? '경매종료시간'.styled(inputHint)
                                    : "경매종료시간".styled(inputHint),
                                SizedBox(height: 10),
                                InkWell(
                                  onTap: () {
                                    selectDate(context);
                                  },
                                  child: TextInputWithPrefixSuffixMaterial(
                                      inputType: TextInputType.number,
                                      enabled: false,
                                      backgroundColor: Colors.grey[100],
                                      borderRadius: 10,
                                      floatingLabelBehavior:
                                          FloatingLabelBehavior.never,
                                      hint: '',
                                      controller: auctionEndTimeController,
                                      validator: (arg) =>
                                          arg.length == 0 ? '종료 시간 필수' : null),
                                ),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                '타입'.styled(inputHint),
                                SizedBox(height: 5),
                                Wrap(
                                  alignment: WrapAlignment.start,
                                  direction: Axis.horizontal,
                                  crossAxisAlignment: WrapCrossAlignment.center,
                                  runAlignment: WrapAlignment.center,
                                  children: <Widget>[
                                    Radio(
                                      toggleable: false,
                                      activeColor: Colors.orange[700],
                                      value: 'buy',
                                      groupValue: radioNotifier.value,
                                      onChanged: radioOnChange,
                                    ),
                                    '사기'.styled(inputHint),
                                    SizedBox(
                                      width: 20,
                                    ),
                                    Radio(
                                      toggleable: false,
                                      activeColor: Colors.orange[700],
                                      value: 'sell',
                                      groupValue: radioNotifier.value,
                                      onChanged: radioOnChange,
                                    ),
                                    '팔기 '.styled(inputHint),
                                  ],
                                ),
                              ],
                            ),
                            SizedBox(height: 10),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                '서술'.styled(inputHint),
                                SizedBox(height: 10),
                                TextAreaWithPrefixSuffixMaterial(
                                  inputType: TextInputType.text,
                                  backgroundColor: Colors.grey[100],
                                  borderRadius: 10,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.never,
                                  hint: '',
                                  controller: descriptionController,
                                ),
                              ],
                            ),
                            SizedBox(height: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                '경매 이미지'.styled(inputHint),
                                SizedBox(height: 8),
                                InkWell(
                                  onTap: () async {
                                    FlutterImagePicker.imagePickerModalSheet(
                                        context: context,
                                        fromCamera: () async {
                                          final imgFile =
                                              await pickImageFromDevice(
                                                  context: context,
                                                  pickerType: 'camera');
                                          uploadImageToS3(
                                            imgFile,
                                          );
                                        },
                                        fromGallery: () async {
                                          final imgFile =
                                              await pickImageFromDevice(
                                                  context: context,
                                                  pickerType: 'gallery');
                                          uploadImageToS3(
                                            imgFile,
                                          );
                                        });
                                  },
                                  child: TextInputWithPrefixSuffixMaterial(
                                    enabled: false,
                                    controller: productImageController,
                                    inputType: TextInputType.text,
                                    backgroundColor: Colors.grey[100],
                                    borderRadius: 10,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                    hint: '',
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Align(
                          alignment: Alignment.center,
                          child: validate()
                              ? loginButton(
                                  context: context,
                                  onPressed: () async {
                                    formLoader.value = true;
                                    var total =
                                        unitPriceController.value.numberValue *
                                            double.parse(
                                                volumeController.value.text);
                                    var auction = {
                                      "auction": {
                                        'kind': radioTypeNotifier.value,
                                        'bid_kind': radioNotifier.value,
                                        'unit_price': unitPriceController
                                            .value.numberValue
                                            .toString(),
                                        'volume': volumeController.value.text,
                                        'start_time': DateTime.now().toString(),
                                        'end_time': DateTime.parse(
                                                auctionEndTimeController.text)
                                            .subtract(
                                                currentDateTime.timeZoneOffset)
                                            .toString(),
                                        'preview_urls': productImageController
                                                    .value.text.length >
                                                0
                                            ? [
                                                productImageController
                                                    .value.text
                                              ]
                                            : null,
                                        'total': total,
                                        'variant_id': dropdownSubcategoryValue
                                            .value['id'],
                                      }
                                    };

                                    var auctionRespose =
                                        await createAuction(auction);
                                    if (auctionRespose['error'] == null) {
                                      FlutterToast.showSuccess(
                                          context: context,
                                          message: auctionRespose['message']);
                                      formLoader.value = false;

                                      Routes.sailor.navigate('/');
                                    } else {
                                      formLoader.value = false;

                                      FlutterToast.showErrorToast(
                                          context: context,
                                          message: auctionRespose['message']);
                                    }
                                  },
                                  text: kind == "normal_auction"
                                      ? '경매개설하기'
                                      : "역경매개설하기",
                                  width: 450)
                              : loginButtonDisabled(
                                  context: context,
                                  onPressed: () {},
                                  text: kind == "normal_auction"
                                      ? '경매개설하기'
                                      : "역경매개설하기",
                                  width: 50),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                      ]),
                    ),
                  ),
                ),
              ),
            ),
          )
        : Loader();
  }
}
