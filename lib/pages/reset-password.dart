import 'package:auctioneer/custom.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/widgets/text_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';

class ResetPassword extends HookWidget {
  const ResetPassword({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final passwordController = useTextEditingController();
    final confirmController = useTextEditingController();

    bool isScreenWide = MediaQuery.of(context).size.width >= 740;

    return Scaffold(
        backgroundColor: Colors.grey[100].withOpacity(0.6),
        body: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Center(
            child: Padding(
              padding: isScreenWide
                  ? EdgeInsets.symmetric(vertical: 100)
                  : EdgeInsets.symmetric(vertical: 0),
              child: Container(
                width: 700,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 2,
                        blurRadius: 2,
                        offset: Offset(0.0, 0.75)),
                  ],
                ),
                child: Container(
                  width: 500,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(height: 20),
                      profileContainer(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            '암호를 재설정'.styled(profileSideHeading(context)),
                            SizedBox(height: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                '비밀번호'.styled(inputHint),
                                SizedBox(height: 8),
                                TextInputWithPrefixSuffixMaterial(
                                    inputType: TextInputType.visiblePassword,
                                    backgroundColor: Colors.grey[100],
                                    borderRadius: 10,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                    hint: '',
                                    controller: passwordController,
                                    validator: (arg) =>
                                        arg.length == 0 ? '비밀번호 필수' : null),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                '비밀번호 확인'.styled(inputHint),
                                SizedBox(height: 10),
                                TextInputWithPrefixSuffixMaterial(
                                    inputType: TextInputType.visiblePassword,
                                    enabled: false,
                                    backgroundColor: Colors.grey[100],
                                    borderRadius: 10,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                    hint: '',
                                    controller: confirmController,
                                    validator: (arg) =>
                                        arg.length == 0 ? '비밀번호 필수' : null),
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: loginButton(
                                  context: context,
                                  onPressed: () => {},
                                  text: '암호를 재설정',
                                  width: 40),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                          ]),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}
