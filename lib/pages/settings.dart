import 'package:auctioneer/core/dark_theme/dark_theme_provider.dart';
import 'package:auctioneer/core/dark_theme/dark_theme_shared_preference.dart';
import 'package:auctioneer/core/local-storage.dart';
import 'package:auctioneer/custom.dart';
import 'package:auctioneer/routes.dart';
import 'package:auctioneer/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';
import 'package:provider/provider.dart';

class Settings extends HookWidget {
  const Settings({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    DarkThemePreference darkmode = new DarkThemePreference();
    final themeChange = Provider.of<DarkThemeProvider>(context);

    final ValueNotifier<bool> darkMode = useValueNotifier(false);
    getDarkTheme() async {
      darkMode.value = await darkmode.getTheme();
    }

    // ignore: missing_return
    useEffect(() {
      getDarkTheme();
    }, []);
    List<Map<String, dynamic>> settings = [
      {
        "icon": settingsIcon(context, Icons.person_outline),
        "title": "Manage My Account",
        "key": "account"
      },
      {
        "icon": settingsIcon(context, Icons.bedtime_rounded),
        "title": "Dark Mode",
        "key": "dark_mode"
      },
    ];

    logout() async {
      await SecureStorage.deleteAllSecureData();
      Routes.sailor.navigate('/sign-in');
    }

    setDarkmode() async {
      darkMode.value = darkMode.value ? false : true;
      themeChange.darkTheme = darkMode.value;
    }

    onSelectSetting(setting) {
      String data = '${setting['key']}';
      switch (data) {
        case 'account':
          return Routes.sailor.navigate('/edit-profile');
        case 'dark_mode':
          return setDarkmode();
        default:
          break;
      }
    }

    useListenable(darkMode);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: commonAppBar(context, 'Settings'),
      bottomNavigationBar: bottomBarBtn(
          context: context, text: 'Logout', onPressed: () => {logout()}),
      body: SafeArea(
        child: ListView(
          children: [
            SizedBox(
              height: 20,
            ),
            Column(
              children: settings.map<Widget>((setting) {
                return InkWell(
                  onTap: () {
                    onSelectSetting(setting);
                  },
                  child: Container(
                    width: double.maxFinite,
                    padding: EdgeInsets.symmetric(vertical: 15, horizontal: 20),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              width: 20,
                              child: setting['icon'],
                            ),
                            SizedBox(
                              width: 20,
                            ),
                            '${setting['title']}'
                                .styled(profileUserNameText(context)),
                          ],
                        ),
                        setting['key'] != 'dark_mode'
                            ? settingsRightArrowIcon(context)
                            : Switch(
                                value: darkMode.value,
                                onChanged: (value) {
                                  darkMode.value = value;
                                  themeChange.darkTheme = darkMode.value;
                                },
                                activeTrackColor: Theme.of(context)
                                    .primaryColorDark
                                    .withOpacity(0.4),
                                activeColor: Theme.of(context).primaryColorDark,
                              ),
                      ],
                    ),
                  ),
                );
              }).toList(),
            )
          ],
        ),
      ),
    );
  }
}
