import 'dart:ui';
import 'package:auctioneer/hooks/auctioneer_bottom_nav_bar.dart';
import 'package:auctioneer/ui/auction-listing.dart';
import 'package:auctioneer/widgets/text_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class Dashboard extends HookWidget {
  const Dashboard({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final searchQuery = useValueNotifier<String>("");
    final tabController = useTabController(initialLength: 3);
    final navBar = useAuctioneerBottomNavBar(context, 0);
    final size = MediaQuery.of(context).size;
    final isList = useValueNotifier(false);
    final searchController = useTextEditingController();

    useListenable(isList);

    useEffect(() {}, []);

    return SafeArea(
      top: false,
      child: Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        bottomNavigationBar: navBar.value.bottomBar,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(60),
          child: Padding(
              padding: EdgeInsets.only(left: 20, right: 20, top: 40),
              child: TextInputSearchMaterial(
                inputType: TextInputType.name,
                hint: 'Search',
                controller: searchController,
                suffixIcon: InkWell(
                  onTap: () {
                    searchQuery.value = searchController.value.text;
                  },
                  child: Padding(
                    padding: EdgeInsets.all(10),
                    child: Icon(Icons.search_sharp),
                  ),
                ),
              )),
        ),
        body: DefaultTabController(
          length: 3,
          child: NestedScrollView(
            headerSliverBuilder: (context, _) {
              return [
                SliverList(
                  delegate: SliverChildListDelegate([]),
                ),
              ];
            },
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: size.width * .74,
                      child: TabBar(
                        indicatorSize: TabBarIndicatorSize.label,
                        controller: tabController,
                        isScrollable: true,
                        unselectedLabelColor:
                            Theme.of(context).primaryColorDark.withOpacity(0.4),
                        labelColor: Theme.of(context).primaryColorDark,
                        indicator: BoxDecoration(color: Colors.transparent),
                        indicatorColor: Color(0xffceb811),
                        indicatorWeight: 0,
                        labelPadding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                        tabs: [
                          Text(
                            'All',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            'Auction',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                          Text(
                            'Reverse Auction',
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              isList.value = false;
                            },
                            child: Icon(
                              Icons.grid_view,
                              color: !isList.value
                                  ? Theme.of(context).primaryColorDark
                                  : Theme.of(context)
                                      .primaryColorDark
                                      .withOpacity(0.4),
                            ),
                          ),
                          SizedBox(width: 5),
                          InkWell(
                            onTap: () {
                              isList.value = true;
                            },
                            child: Icon(
                              Icons.list_alt_outlined,
                              color: isList.value
                                  ? Theme.of(context).primaryColorDark
                                  : Theme.of(context)
                                      .primaryColorDark
                                      .withOpacity(0.4),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Expanded(
                  child: TabBarView(
                    controller: tabController,
                    physics: NeverScrollableScrollPhysics(),
                    children: [
                      SingleChildScrollView(
                        child: AuctionListing(
                          searchQuery: searchQuery,
                          isList: isList.value,
                        ),
                      ),
                      SingleChildScrollView(
                        child: AuctionListing(
                          type: 'kind',
                          query: 'normal_auction',
                          searchQuery: searchQuery,
                          isList: isList.value,
                        ),
                      ),
                      SingleChildScrollView(
                        child: AuctionListing(
                          type: 'kind',
                          query: 'reverse_auction',
                          searchQuery: searchQuery,
                          isList: isList.value,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
