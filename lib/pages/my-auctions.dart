import 'dart:ui';
import 'package:auctioneer/hooks/use-navbar.dart';
import 'package:auctioneer/ui/auction-listing.dart';
import 'package:auctioneer/widgets/side-bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:layout/layout.dart';

class MyAuctions extends HookWidget {
  const MyAuctions({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final navbarHook = useNavbar();
    final ValueNotifier<String> searchQuery = useValueNotifier("");
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: navbarHook.appBar,
      body: Sidebar(
        child: DefaultTabController(
          length: 2,
          child: NestedScrollView(
            headerSliverBuilder: (context, _) {
              return [
                SliverList(
                  delegate: SliverChildListDelegate([]),
                ),
              ];
            },
            body: Column(
              children: <Widget>[
                TabBar(
                  unselectedLabelColor: Theme.of(context).cardColor,
                  labelColor: Colors.orange[700],
                  indicator: BoxDecoration(color: Colors.white),
                  tabs: [
                    new Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        '참가 경매/ 역경매 ',
                        style: TextStyle(
                            fontSize:
                                context.layout.value(xs: 10, sm: 14, md: 20),
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    new Container(
                      alignment: Alignment.centerLeft,
                      child: Text(
                        '내 경매/역경매',
                        style: TextStyle(
                            fontSize:
                                context.layout.value(xs: 10, sm: 14, md: 20),
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                  indicatorColor: Color(0xffceb811),
                  indicatorWeight: 3,
                  labelPadding:
                      EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                ),
                Expanded(
                  child: TabBarView(
                    children: [
                      SingleChildScrollView(
                        child: AuctionListing(
                          type: 'filter',
                          query: 'mine',
                          searchQuery: searchQuery,
                        ),
                      ),
                      SingleChildScrollView(
                        child: AuctionListing(
                          type: 'filter',
                          query: 'participate',
                          searchQuery: searchQuery,
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
