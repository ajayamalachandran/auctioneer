import 'package:auctioneer/hooks/use-navbar.dart';
import 'package:auctioneer/ui/about-us.dart';
import 'package:auctioneer/ui/contact-us.dart';
import 'package:auctioneer/ui/how-it-works.dart';
import 'package:auctioneer/ui/intro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class Landing extends HookWidget {
  const Landing({Key key}) : super(key: key);
  get sections => [];
  @override
  Widget build(BuildContext context) {
    final navbarHook = useNavbar();
    return Scaffold(
        backgroundColor: Theme.of(context).primaryColor,
        drawer:
            MediaQuery.of(context).size.width > 800 ? null : navbarHook.drawer,
        appBar: navbarHook.appBar,
        body: PageView(
          controller: navbarHook.pageController,
          physics: AlwaysScrollableScrollPhysics(),
          scrollDirection: Axis.vertical,
          children: [
            Intro(),
            HowItWorks(),
            AboutUs(),
            ContactUs(),
          ],
        ));
  }
}
