import 'package:auctioneer/components/auction-details/auction-bottom-sheet.dart';
import 'package:auctioneer/components/bid-details/bidders-lists.dart';
import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/core/loader.dart';
import 'package:auctioneer/custom.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/widgets/cureved_bottom_sheet.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';

class AuctionDetails extends HookWidget {
  final auction;
  const AuctionDetails({Key key, this.auction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final auctionNotifier = useValueNotifier({});
    final bids = useValueNotifier([]);
    int endTime = DateTime.parse(auctionNotifier.value['id'] != null
                ? auctionNotifier.value['end_time']
                : DateTime.now().toString())
            .millisecondsSinceEpoch +
        1000 * 30;

    bool isAuctionEnded() {
      return DateTime.now().millisecondsSinceEpoch >
          DateTime.parse(auctionNotifier.value['id'] != null
                  ? auctionNotifier.value['end_time']
                  : DateTime.now().toString())
              .millisecondsSinceEpoch;
    }

    useEffect(() {
      getAuction(auction['id']).pipe(auctionNotifier);
      getAuctionBids(auction['id']).pipe(bids);
    }, []);

    useListenable(auctionNotifier);
    useListenable(bids);

    return auctionNotifier.value['id'] != null
        ? Scaffold(
            extendBodyBehindAppBar: true,
            backgroundColor: Theme.of(context).backgroundColor,
            floatingActionButton: !isAuctionEnded()
                ? floatingActionButtonEnabled(
                    onTap: () {
                      curvedBottomSheet(
                          context: context,
                          child: AuctionDetailsBottomsheet(
                            auction: auctionNotifier.value,
                          ));
                    },
                    context: context,
                    text: 'Place Bid',
                    width: size.width * .8,
                  )
                : floatingActionButtonDisabled(
                    onTap: () {},
                    context: context,
                    text: 'Place Bid',
                    width: size.width * .8,
                  ),
            floatingActionButtonLocation:
                FloatingActionButtonLocation.centerFloat,
            body: NestedScrollView(
              headerSliverBuilder: (context, _) {
                return [
                  SliverList(
                    delegate: SliverChildListDelegate([
                      CachedNetworkImage(
                        imageUrl: auctionNotifier.value['preview_urls'] !=
                                    null &&
                                auctionNotifier.value['preview_urls'].length > 0
                            ? auctionNotifier.value['preview_urls'][0]
                            : "https://i.ibb.co/V9bBvnB/rods.jpg",
                        imageBuilder: (context, imageProvider) {
                          return Container(
                            width: size.width,
                            height: 300,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: imageProvider, fit: BoxFit.cover),
                            ),
                            child: Stack(
                              clipBehavior: Clip.none,
                              children: [
                                Positioned(
                                  top: 20,
                                  child: Container(
                                    width: size.width,
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 20, vertical: 20),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        InkWell(
                                          onTap: () {
                                            Navigator.pop(context);
                                          },
                                          child: Container(
                                            padding: EdgeInsets.all(5),
                                            decoration: BoxDecoration(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                color: Theme.of(context)
                                                    .backgroundColor),
                                            child: Icon(Icons.arrow_back_ios),
                                          ),
                                        ),
                                        Row(
                                          children: [
                                            Container(
                                              padding: EdgeInsets.all(5),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color: Theme.of(context)
                                                      .backgroundColor),
                                              child: Icon(Icons
                                                  .favorite_border_outlined),
                                            ),
                                            SizedBox(width: 10),
                                            Container(
                                              padding: EdgeInsets.all(5),
                                              decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  color: Theme.of(context)
                                                      .backgroundColor),
                                              child: Icon(Icons.ios_share),
                                            ),
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                Positioned.fill(
                                  bottom: -25,
                                  child: Align(
                                    alignment: Alignment.bottomCenter,
                                    child: Container(
                                      width: size.width * .7,
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 20, vertical: 20),
                                      decoration: BoxDecoration(
                                        color: Theme.of(context).cardColor,
                                        borderRadius: BorderRadius.circular(10),
                                        boxShadow: [
                                          BoxShadow(
                                            color:
                                                Colors.black.withOpacity(0.2),
                                            blurRadius: 6.0,
                                            spreadRadius: 2.0,
                                            offset: Offset(
                                              5.0,
                                              5.0,
                                            ),
                                          )
                                        ],
                                      ),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Column(
                                            mainAxisSize: MainAxisSize.min,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              '\$ 50'.styled(
                                                  auctionCountDownStyle(
                                                      context)),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              'Current Bid'.styled(
                                                  auctionDescriptionStyle)
                                            ],
                                          ),
                                          Column(
                                            mainAxisSize: MainAxisSize.min,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.end,
                                            children: [
                                              CountdownTimer(
                                                endTime: endTime,
                                                widgetBuilder: (_,
                                                    CurrentRemainingTime time) {
                                                  if (time == null) {
                                                    return auctionCountdown(
                                                        context: context,
                                                        icon: Icons.timer,
                                                        color: Theme.of(context)
                                                            .primaryColorDark,
                                                        label: " Finished");
                                                  } else {
                                                    return auctionCountdown(
                                                        context: context,
                                                        icon: Icons.timer,
                                                        color: Theme.of(context)
                                                            .primaryColorDark,
                                                        label:
                                                            " ${time.days != null ? time.days : 0}d ${time.hours != null ? time.hours : 0}h ${time.min != null ? time.min : 0}m ${time.sec != null ? time.sec : 0}s");
                                                  }
                                                },
                                              ),
                                              SizedBox(
                                                height: 10,
                                              ),
                                              'Auction Ends'.styled(
                                                  auctionDescriptionStyle)
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                      Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: 30,
                            ),
                            Container(
                              width: 260,
                              child: auctionNotifier.value['name']
                                  .toString()
                                  .styled(auctionTitleStyle(context)),
                            ),
                            Container(
                              width: 400,
                              child:
                                  'Made from Highest quality food grade stainless steel and Mirror finish'
                                      .styled(auctionDescriptionStyle),
                            ),
                          ],
                        ),
                      ),
                      Divider(),
                    ]),
                  ),
                ];
              },
              body: Column(
                children: <Widget>[
                  Expanded(
                      child: BiddersList(
                    auction: auctionNotifier.value,
                  )),
                ],
              ),
            ),
          )
        : Loader();
  }
}
