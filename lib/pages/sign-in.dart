import 'package:auctioneer/components/auth/signin-bottomsheet.dart';
import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/core/flutter-toast.dart';
import 'package:auctioneer/core/loader.dart';
import 'package:auctioneer/core/local-storage.dart';
import 'package:auctioneer/custom.dart';
import 'package:auctioneer/routes.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/widgets/text_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';

class SignIn extends HookWidget {
  const SignIn({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final email = useTextEditingController();
    final password = useTextEditingController();
    final formLoader = useValueNotifier(false);
    final size = MediaQuery.of(context).size;
    useListenable(email);
    useListenable(password);
    useListenable(formLoader);
    return !formLoader.value
        ? SignInBackground(
            child: Container(
              height: size.height * .7,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30),
                  )),
              child: Padding(
                padding: EdgeInsets.all(20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    'Welcome to Auctioneer !'.styled(welcomeTitle),
                    SizedBox(height: 10),
                    'Login to continue'.styled(profileSideHeading(context)),
                    SizedBox(height: 20),
                    TextInputWithPrefixSuffixMaterial(
                      inputType: TextInputType.text,
                      borderRadius: 10,
                      floatingLabelBehavior: FloatingLabelBehavior.auto,
                      hint: 'Email',
                      controller: email,
                      validator: (arg) =>
                          arg.length == 0 ? 'Email required' : null,
                      prefixItem: Icon(
                        Icons.mail_outline_outlined,
                        size: 20,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(height: 20),
                    TextInputWithPrefixSuffixMaterial(
                      obscureText: true,
                      inputType: TextInputType.visiblePassword,
                      borderRadius: 10,
                      floatingLabelBehavior: FloatingLabelBehavior.auto,
                      hint: 'Password',
                      controller: password,
                      validator: (arg) =>
                          arg.length == 0 ? 'Password required' : null,
                      prefixItem: Icon(
                        Icons.lock_outline,
                        size: 20,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: email.value.text.length > 0 &&
                              password.value.text.length > 0
                          ? loginButton(
                              context: context,
                              onPressed: () async {
                                formLoader.value = true;
                                final loginResponse = await login({
                                  'user': {
                                    'email': email.value.text,
                                    'password': password.value.text
                                  }
                                });
                                if (loginResponse['error'] == null) {
                                  await SecureStorage.saveToken(
                                      loginResponse['token']);

                                  await SecureStorage.writeSecureDataLocally(
                                      key: 'user',
                                      data: loginResponse.toString());

                                  Routes.sailor.navigate('/');

                                  formLoader.value = false;
                                } else {
                                  formLoader.value = false;
                                  FlutterToast.showErrorToast(
                                      context: context,
                                      message: loginResponse['message']);
                                }
                              },
                              text: 'Login',
                              width: 300)
                          : loginButtonDisabled(
                              context: context,
                              onPressed: () => {},
                              text: 'Login',
                              width: 300),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Align(
                      alignment: Alignment.center,
                      child: InkWell(
                        onTap: () => {
                          Routes.sailor.navigate('/sign-up'),
                        },
                        child: 'New here ? Signup'.styled(forgetPasswordText),
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            width: size.width * .35,
                            child: Divider(),
                          ),
                          'Or'.styled(signinOptionsText),
                          Container(
                            width: size.width * .35,
                            child: Divider(),
                          ),
                        ]),
                    SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        InkWell(child: googleNewIcon),
                        InkWell(onTap: () async {}, child: fbNewIcon),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          )
        : Loader();
  }
}
