import 'package:auctioneer/custom.dart';
import 'package:auctioneer/routes.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/widgets/text_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';

class ForgotPassword extends HookWidget {
  const ForgotPassword({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final emailController = useTextEditingController();

    bool isScreenWide = MediaQuery.of(context).size.width >= 740;

    return Scaffold(
        backgroundColor: Colors.grey[100].withOpacity(0.6),
        body: Form(
          autovalidateMode: AutovalidateMode.onUserInteraction,
          child: Center(
            child: Padding(
              padding: isScreenWide
                  ? EdgeInsets.symmetric(vertical: 30)
                  : EdgeInsets.symmetric(vertical: 0),
              child: Container(
                width: 700,
                decoration: BoxDecoration(
                  color: Theme.of(context).cardColor,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 2,
                        blurRadius: 2,
                        offset: Offset(0.0, 0.75)),
                  ],
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(height: 20),
                    Container(
                      width: 500,
                      child: profileContainer(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            '비밀번호 분실'.styled(profileSideHeading(context)),
                            SizedBox(height: 20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                '이메일'.styled(inputHint),
                                SizedBox(height: 8),
                                TextInputWithPrefixSuffixMaterial(
                                    inputType: TextInputType.text,
                                    backgroundColor: Colors.grey[100],
                                    borderRadius: 10,
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.never,
                                    hint: '',
                                    controller: emailController,
                                    validator: (arg) =>
                                        arg.length == 0 ? '이메일 필수' : null),
                              ],
                            ),
                            SizedBox(
                              height: 30,
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: loginButton(
                                  context: context,
                                  onPressed: () => {
                                        Routes.sailor
                                            .navigate('/reset-password')
                                      },
                                  text: '링크 보내기 (비밀번호 변경)',
                                  width: 300),
                            ),
                            SizedBox(
                              height: 30,
                            ),
                          ]),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
