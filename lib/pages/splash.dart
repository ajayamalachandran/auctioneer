import 'package:auctioneer/core/loader.dart';
import 'package:auctioneer/core/local-storage.dart';
import 'package:auctioneer/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class Splash extends HookWidget {
  const Splash({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    useFuture(
      Future.delayed(
        Duration(milliseconds: 0),
        () async {
          final token = await SecureStorage.getToken();
          return Routes.sailor.navigate(token != null ? "/" : "/sign-in");
        },
      ),
    );
    return Scaffold(
      body: Center(child: Loader()),
    );
  }
}
