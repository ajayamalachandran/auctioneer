import 'package:clipboard/clipboard.dart';
import 'package:auctioneer/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';
import 'package:layout/layout.dart';

class ContactUs extends HookWidget {
  const ContactUs({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final clipBoardStatus = useValueNotifier('Copy');
    useListenable(clipBoardStatus);

    return new Material(
        color: Colors.black.withAlpha(200),
        child: Center(child: LayoutBuilder(builder: (context, constraints) {
          return Container(
              color: Colors.white,
              width: context.breakpoint > LayoutBreakpoint.sm
                  ? size.width * .4
                  : size.width,
              height: context.breakpoint > LayoutBreakpoint.sm
                  ? size.height * .5
                  : size.height,
              child: new GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Column(
                  children: [
                    Container(
                        height: size.height * .3,
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image:
                                    AssetImage('assets/contact_us_banner.jpg'),
                                fit: BoxFit.cover)),
                        child: Align(
                          alignment: Alignment.topRight,
                          child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Container(
                              height: 20,
                              width: 20,
                              color: Colors.white,
                              alignment: Alignment.center,
                              child: Icon(
                                Icons.close,
                                size: 16,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        )),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              onTap: () {
                                FlutterClipboard.copy('cso@trilionslab.com')
                                    .then((value) =>
                                        {clipBoardStatus.value = 'Copied'});
                              },
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  'Email'.styled(btnLabelStyle),
                                  'cso@trilionslab.com'.styled(btnLabelStyle),
                                  clipBoardStatus.value.styled(btnLabelStyle),
                                ],
                              ),
                            ),
                            SizedBox(height: 10),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                'Tel'.styled(btnLabelStyle),
                                '010-2726-1258'.styled(btnLabelStyle),
                                ''.styled(btnLabelStyle),
                              ],
                            )
                          ],
                        )),
                  ],
                ),
              ));
        })));
  }
}
