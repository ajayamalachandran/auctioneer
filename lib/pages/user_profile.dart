import 'package:auctioneer/canvas/edit-profile-painter.dart';
import 'package:auctioneer/components/user_profile/user-auction-count.dart';
import 'package:auctioneer/components/user_profile/user_profile_basic_info.dart';
import 'package:auctioneer/hooks/auctioneer_bottom_nav_bar.dart';
import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/core/loader.dart';
import 'package:auctioneer/custom.dart';
import 'package:auctioneer/routes.dart';
import 'package:auctioneer/ui/auction-listing.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class UserProfile extends HookWidget {
  const UserProfile({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final navBar = useAuctioneerBottomNavBar(context, 3);
    final size = MediaQuery.of(context).size;
    final formLoader = useValueNotifier(false);
    final userInfo = useValueNotifier({});
    final ValueNotifier<String> searchQuery = useValueNotifier("");

    final animationController = useAnimationController();
    final curve =
        CurvedAnimation(parent: animationController, curve: Curves.easeIn);
    final tabController = useTabController(initialLength: 2);
    final isList = useValueNotifier(false);

    final tweenAnimation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(curve);

    final animation = useAnimation(tweenAnimation);

    getMyProfileInfo() async {
      userInfo.value = await getMe();
    }

    useEffect(() {
      getMyProfileInfo();
      animationController.animateTo(1.0, duration: Duration(milliseconds: 0))
        ..then<TickerFuture>((value) => animationController.animateBack(1.0,
            duration: Duration(milliseconds: 1000)));
    }, []);
    useListenable(userInfo);
    useListenable(formLoader);
    useListenable(isList);

    return !formLoader.value && userInfo.value['id'] != null
        ? Scaffold(
            backgroundColor: Theme.of(context).backgroundColor,
            bottomNavigationBar: navBar.value.bottomBar,
            appBar: appBarWithActions(
                context: context,
                text: '${userInfo.value['name']}',
                actions: [
                  InkWell(
                    onTap: () {
                      Routes.sailor.navigate('/settings');
                    },
                    child: Padding(
                      padding: EdgeInsets.all(10),
                      child: Icon(
                        Icons.settings_outlined,
                        size: 16,
                        color: Theme.of(context).primaryColorDark,
                      ),
                    ),
                  ),
                ]),
            body: DefaultTabController(
              length: 2,
              initialIndex: 0,
              child: NestedScrollView(
                headerSliverBuilder: (context, _) {
                  return [
                    SliverList(
                      delegate: SliverChildListDelegate([
                        Stack(
                          children: [
                            CustomPaint(
                              size: Size(
                                  size.width,
                                  (size.width * 0.625)
                                      .toDouble()), //You can Replace [WIDTH] with your desired width for Custom Paint and height will be calculated automatically
                              painter: EditProfileCanvas(),
                            ),
                            Align(
                              alignment: Alignment.center,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: size.height * 0.0340000,
                                  ),
                                  UserProfileBasicInfo(
                                      userInfo: userInfo.value),
                                  SizedBox(
                                    height: 40,
                                  ),
                                  UserAuctionCount(userInfo: userInfo),
                                  SizedBox(
                                    height: 20,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ]),
                    ),
                  ];
                },
                body: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          width: size.width * .74,
                          child: TabBar(
                            indicatorSize: TabBarIndicatorSize.label,
                            controller: tabController,
                            isScrollable: true,
                            unselectedLabelColor: Theme.of(context)
                                .primaryColorDark
                                .withOpacity(0.4),
                            labelColor: Theme.of(context).primaryColorDark,
                            indicator: BoxDecoration(color: Colors.transparent),
                            indicatorColor: Color(0xffceb811),
                            indicatorWeight: 0,
                            labelPadding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 20),
                            tabs: [
                              Text(
                                'Auctions',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                'Reverse Auctions',
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Row(
                            children: [
                              InkWell(
                                onTap: () {
                                  isList.value = false;
                                },
                                child: Icon(
                                  Icons.grid_view,
                                  color: !isList.value
                                      ? Theme.of(context).primaryColorDark
                                      : Theme.of(context)
                                          .primaryColorDark
                                          .withOpacity(0.4),
                                ),
                              ),
                              SizedBox(width: 5),
                              InkWell(
                                onTap: () {
                                  isList.value = true;
                                },
                                child: Icon(
                                  Icons.list_alt_outlined,
                                  color: isList.value
                                      ? Theme.of(context).primaryColorDark
                                      : Theme.of(context)
                                          .primaryColorDark
                                          .withOpacity(0.4),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Expanded(
                      child: TabBarView(
                        controller: tabController,
                        children: [
                          SingleChildScrollView(
                            child: AuctionListing(
                              type: 'filter',
                              query: 'mine',
                              searchQuery: searchQuery,
                              isList: isList.value,
                            ),
                          ),
                          SingleChildScrollView(
                            child: AuctionListing(
                              type: 'filter',
                              query: 'participate',
                              searchQuery: searchQuery,
                              isList: isList.value,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ))
        : Loader();
  }
}
