import 'dart:io';
import 'package:auctioneer/canvas/edit-profile-painter.dart';
import 'package:auctioneer/core/tools/Image_picker_methods.dart';
import 'package:auctioneer/hooks/auctioneer_bottom_nav_bar.dart';
import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/core/flutter-toast.dart';
import 'package:auctioneer/core/loader.dart';
import 'package:auctioneer/custom.dart';
import 'package:auctioneer/routes.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/widgets/text_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';
import 'package:auctioneer/components/user_profile/user_profile_header.dart';
import 'package:im_stepper/stepper.dart';
import 'package:path_provider/path_provider.dart';

class EditProfile extends HookWidget {
  const EditProfile({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final navBar = useAuctioneerBottomNavBar(context, 3);
    final radioNotifier = useValueNotifier(0);
    final activeStep = useValueNotifier(0);
    final size = MediaQuery.of(context).size;
    final formLoader = useValueNotifier(false);
    final avatarUrl = useValueNotifier('');
    final usernameController = useTextEditingController();
    final phonenumberController = useTextEditingController();
    final emailController = useTextEditingController();
    final accountNumberController = useTextEditingController();
    final bankNameusernameController = useTextEditingController();
    final accountHolderNameController = useTextEditingController();
    final companyNameController = useTextEditingController();
    final representativeNameController = useTextEditingController();
    final companyRegistrationNumberController = useTextEditingController();
    final companyImageController = useTextEditingController();
    final companyAddressController = useTextEditingController();
    final animationController = useAnimationController();
    final curve =
        CurvedAnimation(parent: animationController, curve: Curves.easeIn);

    final tweenAnimation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(curve);

    final animation = useAnimation(tweenAnimation);
    useListenable(radioNotifier);
    useListenable(usernameController);
    useListenable(phonenumberController);
    useListenable(emailController);
    useListenable(accountNumberController);
    useListenable(bankNameusernameController);
    useListenable(accountHolderNameController);
    useListenable(companyNameController);
    useListenable(representativeNameController);
    useListenable(companyRegistrationNumberController);
    useListenable(avatarUrl);
    useListenable(companyImageController);
    useListenable(formLoader);
    useListenable(activeStep);

    bool validate() {
      return radioNotifier.value != null &&
          usernameController.value.text.length > 0 &&
          phonenumberController.value.text.length > 0 &&
          emailController.value.text.length > 0 &&
          accountNumberController.value.text.length > 0 &&
          bankNameusernameController.value.text.length > 0 &&
          accountHolderNameController.value.text.length > 0 &&
          companyNameController.value.text.length > 0 &&
          representativeNameController.value.text.length > 0 &&
          companyRegistrationNumberController.value.text.length > 0;
    }

    radioOnChange(value) {
      radioNotifier.value = value;
    }

    getMyProfileInfo() async {
      final meResponse = await getMe();
      usernameController.text = meResponse['name'];
      phonenumberController.text = meResponse['phone_number'];
      emailController.text = meResponse['email'];
      accountNumberController.text = meResponse['bank_account_number'];
      bankNameusernameController.text = meResponse['bank_name'];
      accountHolderNameController.text = meResponse['account_holder_name'];
      companyNameController.text = meResponse['company_name'];
      representativeNameController.text = meResponse['representative_name'];
      companyRegistrationNumberController.text =
          meResponse['company_registration_number'];
      companyRegistrationNumberController.text =
          meResponse['company_registration_number'];
      avatarUrl.value = meResponse['avatar_url'];
      companyImageController.text = meResponse['company_registration_doc_url'];
      companyAddressController.text = meResponse['office_address'];
    }

    useEffect(() {
      getMyProfileInfo();
      animationController.animateTo(1.0, duration: Duration(milliseconds: 0))
        ..then<TickerFuture>((value) => animationController.animateBack(1.0,
            duration: Duration(milliseconds: 1000)));
    }, []);

    uploadImageToS3(File image, String type) async {
      formLoader.value = true;

      Directory tempDir = await getTemporaryDirectory();
      String tempPath = tempDir.path;
      File compressedImage =
          await imageCompression(image, tempPath + 'compressed.jpg');
      final url = await getSignedUrl(compressedImage.path);
      if (url['signedUrl'] != null) {
        final img = await uploadImage(url['signedUrl'], compressedImage);
        type == 'profile_image'
            ? avatarUrl.value = img
            : companyImageController.text = img;
        formLoader.value = false;
      } else {
        formLoader.value = false;
      }
    }

    updateMyProfile() async {
      formLoader.value = true;
      var user = {
        'user': {
          "name": usernameController.value.text,
          "email": emailController.value.text,
          "bank_name": bankNameusernameController.value.text,
          "bank_account_number": accountNumberController.value.text,
          "account_holder_name": accountHolderNameController.value.text,
          "business_kind": radioNotifier.value == 0
              ? 'private_business'
              : 'corporate_business',
          'avatar_url': avatarUrl.value.length > 0 ? avatarUrl.value : null,
          'phone_number': phonenumberController.value.text,
          'company_name': companyNameController.value.text,
          'representative_name': representativeNameController.value.text,
          'company_registration_number':
              companyRegistrationNumberController.value.text,
          'company_registration_doc_url':
              companyImageController.value.text.length > 0
                  ? companyImageController.value.text
                  : null,
          'office_address': companyAddressController.value.text
        }
      };

      final profileRespose = await updateProfile(user);
      if (profileRespose['error'] == null) {
        FlutterToast.showSuccess(
            context: context, message: profileRespose['message']);
        formLoader.value = false;

        Navigator.pop(context);
        Routes.sailor.navigate('/');
      } else {
        formLoader.value = false;

        FlutterToast.showErrorToast(
            context: context, message: profileRespose['message']);
      }
    }

    return !formLoader.value
        ? Scaffold(
            backgroundColor: Theme.of(context).backgroundColor,
            bottomNavigationBar: navBar.value.bottomBar,
            appBar: commonAppBar(context, 'Edit profile'),
            floatingActionButton: validate()
                ? loginButton(
                    context: context,
                    onPressed: () async {
                      await updateMyProfile();
                    },
                    text: 'Update',
                    width: 40)
                : loginButtonDisabled(
                    context: context,
                    onPressed: () {},
                    text: 'Update',
                    width: 40),
            floatingActionButtonLocation: FloatingActionButtonLocation.endFloat,
            body: Stack(
              children: [
                CustomPaint(
                  size: Size(size.width, (size.width * 0.625).toDouble()),
                  painter: EditProfileCanvas(),
                ),
                Container(
                  height: size.height,
                  width: size.width,
                  child: Row(
                    children: [
                      IconStepper(
                        enableNextPreviousButtons: false,
                        activeStepColor: Colors.white,
                        activeStepBorderWidth: 2,
                        stepColor: Theme.of(context).primaryColorDark,
                        activeStepBorderColor:
                            Theme.of(context).primaryColorDark,
                        enableStepTapping: true,
                        activeStep: activeStep.value,
                        stepRadius: 20,
                        lineColor: Theme.of(context).primaryColorDark,
                        direction: Axis.vertical,
                        icons: [
                          Icon(
                            Icons.person_outline,
                            size: 13,
                            color: activeStep.value == 0
                                ? Theme.of(context).primaryColorDark
                                : Colors.white,
                          ),
                          Icon(Icons.account_balance_outlined,
                              size: 13,
                              color: activeStep.value == 1
                                  ? Theme.of(context).primaryColorDark
                                  : Colors.white),
                          Icon(Icons.business_center_outlined,
                              size: 13,
                              color: activeStep.value == 2
                                  ? Theme.of(context).primaryColorDark
                                  : Colors.white),
                        ],
                        onStepReached: (index) {
                          animationController
                              .animateTo(0.0,
                                  duration: Duration(milliseconds: 0))
                              .then<TickerFuture>((value) =>
                                  animationController.animateBack(1.0,
                                      duration: Duration(milliseconds: 1000)));
                          activeStep.value = index;
                        },
                      ),
                      FadeTransition(
                        opacity: animationController,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            activeStep.value == 0
                                ? InkWell(
                                    onTap: () async {
                                      FlutterImagePicker.imagePickerModalSheet(
                                          context: context,
                                          fromCamera: () async {
                                            final imgFile =
                                                await pickImageFromDevice(
                                                    context: context,
                                                    pickerType: 'camera');
                                            uploadImageToS3(
                                                imgFile, 'profile_image');
                                          },
                                          fromGallery: () async {
                                            final imgFile =
                                                await pickImageFromDevice(
                                                    context: context,
                                                    pickerType: 'gallery');
                                            uploadImageToS3(
                                                imgFile, 'profile_image');
                                          });
                                    },
                                    child: UserProfileHeader(
                                      imageUrl: avatarUrl.value != null &&
                                              avatarUrl.value.length > 0
                                          ? avatarUrl.value
                                          : "https://i.ibb.co/V9bBvnB/rods.jpg",
                                    ),
                                  )
                                : Container(),
                            activeStep.value == 0
                                ? Column(
                                    children: [
                                      TextInputWithPrefixSuffixMaterial(
                                          width: size.width * .7,
                                          inputType: TextInputType.text,
                                          borderRadius: 10,
                                          floatingLabelBehavior:
                                              FloatingLabelBehavior.auto,
                                          hint: 'Username',
                                          controller: usernameController,
                                          validator: (arg) => arg.length == 0
                                              ? '성명 필요한'
                                              : null),
                                      SizedBox(height: 10),
                                      TextInputWithPrefixSuffixMaterial(
                                          width: size.width * .7,
                                          inputType: TextInputType.text,
                                          borderRadius: 10,
                                          floatingLabelBehavior:
                                              FloatingLabelBehavior.auto,
                                          hint: 'Phone number',
                                          controller: phonenumberController,
                                          validator: (arg) => arg.length == 0
                                              ? '전화번호 필요한'
                                              : null),
                                      SizedBox(height: 10),
                                      TextInputWithPrefixSuffixMaterial(
                                          width: size.width * .7,
                                          inputType: TextInputType.text,
                                          borderRadius: 10,
                                          floatingLabelBehavior:
                                              FloatingLabelBehavior.auto,
                                          hint: 'e-mail',
                                          controller: emailController,
                                          validator: (arg) => arg.length == 0
                                              ? '이메일 필요한'
                                              : null),
                                    ],
                                  )
                                : activeStep.value == 1
                                    ? Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            children: <Widget>[
                                              Radio(
                                                activeColor: Theme.of(context)
                                                    .primaryColorDark,
                                                value: 0,
                                                groupValue: radioNotifier.value,
                                                onChanged: radioOnChange,
                                              ),
                                              'Private'.styled(inputHint),
                                              SizedBox(width: 10),
                                              Radio(
                                                activeColor: Theme.of(context)
                                                    .primaryColorDark,
                                                value: 1,
                                                groupValue: radioNotifier.value,
                                                onChanged: radioOnChange,
                                              ),
                                              'Corporate'.styled(inputHint),
                                            ],
                                          ),
                                          SizedBox(height: 10),
                                          TextInputWithPrefixSuffixMaterial(
                                              width: size.width * .7,
                                              inputType: TextInputType.text,
                                              borderRadius: 10,
                                              floatingLabelBehavior:
                                                  FloatingLabelBehavior.auto,
                                              hint: 'name of bank',
                                              controller:
                                                  bankNameusernameController,
                                              validator: (arg) =>
                                                  arg.length == 0
                                                      ? '은행명 필요한'
                                                      : null),
                                          SizedBox(height: 10),
                                          TextInputWithPrefixSuffixMaterial(
                                              width: size.width * .7,
                                              inputType: TextInputType.number,
                                              borderRadius: 10,
                                              floatingLabelBehavior:
                                                  FloatingLabelBehavior.auto,
                                              hint: 'Account Number',
                                              controller:
                                                  accountNumberController,
                                              validator: (arg) =>
                                                  arg.length == 0
                                                      ? '계좌번호 필요한'
                                                      : null),
                                          SizedBox(height: 10),
                                          TextInputWithPrefixSuffixMaterial(
                                              width: size.width * .7,
                                              inputType: TextInputType.text,
                                              borderRadius: 10,
                                              floatingLabelBehavior:
                                                  FloatingLabelBehavior.auto,
                                              hint: 'Account name',
                                              controller:
                                                  accountHolderNameController,
                                              validator: (arg) =>
                                                  arg.length == 0
                                                      ? '계좌명 필요한'
                                                      : null),
                                        ],
                                      )
                                    : Column(
                                        children: [
                                          TextInputWithPrefixSuffixMaterial(
                                              width: size.width * .7,
                                              inputType: TextInputType.text,
                                              borderRadius: 10,
                                              floatingLabelBehavior:
                                                  FloatingLabelBehavior.auto,
                                              hint: 'Company Name',
                                              controller: companyNameController,
                                              validator: (arg) =>
                                                  arg.length == 0
                                                      ? '은행명 필요한'
                                                      : null),
                                          SizedBox(height: 10),
                                          TextInputWithPrefixSuffixMaterial(
                                              width: size.width * .7,
                                              inputType: TextInputType.number,
                                              borderRadius: 10,
                                              floatingLabelBehavior:
                                                  FloatingLabelBehavior.auto,
                                              hint: 'Representative name',
                                              controller:
                                                  accountNumberController,
                                              validator: (arg) =>
                                                  arg.length == 0
                                                      ? '계좌번호 필요한'
                                                      : null),
                                          SizedBox(height: 10),
                                          TextInputWithPrefixSuffixMaterial(
                                              width: size.width * .7,
                                              inputType: TextInputType.text,
                                              borderRadius: 10,
                                              floatingLabelBehavior:
                                                  FloatingLabelBehavior.auto,
                                              hint:
                                                  'Company Registration Number',
                                              controller:
                                                  accountHolderNameController,
                                              validator: (arg) =>
                                                  arg.length == 0
                                                      ? '계좌명 필요한'
                                                      : null),
                                          SizedBox(height: 10),
                                          TextInputWithPrefixSuffixMaterial(
                                              width: size.width * .7,
                                              inputType: TextInputType.text,
                                              borderRadius: 10,
                                              floatingLabelBehavior:
                                                  FloatingLabelBehavior.auto,
                                              hint:
                                                  'Company Registration Number',
                                              controller:
                                                  accountHolderNameController,
                                              validator: (arg) =>
                                                  arg.length == 0
                                                      ? '계좌명 필요한'
                                                      : null),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          InkWell(
                                            onTap: () async {
                                              FlutterImagePicker
                                                  .imagePickerModalSheet(
                                                      context: context,
                                                      fromCamera: () async {
                                                        final imgFile =
                                                            await pickImageFromDevice(
                                                                context:
                                                                    context,
                                                                pickerType:
                                                                    'camera');
                                                        uploadImageToS3(imgFile,
                                                            'company_profile');
                                                      },
                                                      fromGallery: () async {
                                                        final imgFile =
                                                            await pickImageFromDevice(
                                                                context:
                                                                    context,
                                                                pickerType:
                                                                    'gallery');
                                                        uploadImageToS3(imgFile,
                                                            'company_profile');
                                                      });
                                            },
                                            child:
                                                TextInputWithPrefixSuffixMaterial(
                                              width: size.width * .7,
                                              enabled: false,
                                              controller:
                                                  companyImageController,
                                              inputType: TextInputType.text,
                                              borderRadius: 10,
                                              floatingLabelBehavior:
                                                  FloatingLabelBehavior.auto,
                                              hint:
                                                  'Business registration certificate',
                                            ),
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                          TextAreaWithPrefixSuffixMaterial(
                                            width: size.width * .7,
                                            inputType: TextInputType.text,
                                            borderRadius: 10,
                                            floatingLabelBehavior:
                                                FloatingLabelBehavior.auto,
                                            hint: 'Company Address',
                                            controller:
                                                companyAddressController,
                                          ),
                                        ],
                                      )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        : Loader();
  }
}
