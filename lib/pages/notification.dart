import 'package:auctioneer/components/notifications/notifications-card.dart';
import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/core/loader.dart';
import 'package:auctioneer/hooks/auctioneer_bottom_nav_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/extensions.dart';

class NotificationPage extends HookWidget {
  const NotificationPage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final navBar = useAuctioneerBottomNavBar(context, 1);
    final unreadNotifier = useValueNotifier(0);
    final notifications = useValueNotifier([]);

    useEffect(() {
      getMyNotifications().pipe(notifications);
      unreadNotifier.value = notifications.value
          .where((notification) => notification['read_at'] == null)
          .length;
    }, []);

    useListenable(notifications);
    useListenable(unreadNotifier);
    return notifications.value != null
        ? Scaffold(
            backgroundColor: Theme.of(context).backgroundColor,
            bottomNavigationBar: navBar.value.bottomBar,
            appBar: AppBar(
              elevation: 0,
              backgroundColor: Theme.of(context).backgroundColor,
              leading: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    color: Theme.of(context).primaryColorDark,
                    size: 14,
                  )),
              title: Row(
                children: [
                  'Notifications'.styled(notificationAppBarTitle(context)),
                  unreadNotifier.value > 0
                      ? SizedBox(width: 10)
                      : SizedBox(width: 0),
                  unreadNotifier.value > 0
                      ? Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 5, vertical: 3),
                          decoration: BoxDecoration(
                              color: Theme.of(context).primaryColorDark,
                              borderRadius: BorderRadius.circular(10)),
                          child:
                              '${notifications.value.where((notification) => notification['read_at']).length}'
                                  .styled(notificationNew),
                        )
                      : Container(),
                ],
              ),
            ),
            body: SingleChildScrollView(
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Wrap(
                  spacing: 10,
                  direction: Axis.horizontal,
                  children: notifications.value.map<Widget>((notification) {
                    return notification['auction'] != null
                        ? NotificationCard(
                            unitsAvailable: notification['auction']
                                ['unit_price'],
                            name: notification['user']['name'],
                            price: notification['auction']['total'],
                            imageUrl: notification['bid'] != null &&
                                    notification['bid']['user']['avatar_url'] !=
                                        null
                                ? notification['bid']['user']['avatar_url']
                                : notification['user'] != null
                                    ? notification['user']['avatar_url']
                                    : 'https://i.ibb.co/WtmBb70/metal-tubes.jpg',
                            date: notification['auction']['start_time'],
                            kind: notification['kind'],
                            message: notification['message'],
                            notificationInfo: notification)
                        : Container();
                  }).toList(),
                ),
              ),
            ))
        : Loader();
  }
}
