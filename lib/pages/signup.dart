import 'package:auctioneer/canvas/edit-profile-painter.dart';
import 'package:auctioneer/components/auth/signin-bottomsheet.dart';
import 'package:auctioneer/core/loader.dart';
import 'package:auctioneer/core/tools/Image_picker_methods.dart';
import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/core/flutter-toast.dart';
import 'package:auctioneer/custom.dart';
import 'package:auctioneer/routes.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/widgets/text_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';
import 'package:auctioneer/components/user_profile/user_profile_header.dart';

class SignUp extends HookWidget {
  const SignUp({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final radioNotifier = useValueNotifier(0);
    final activeStep = useValueNotifier(0);
    final size = MediaQuery.of(context).size;
    final formLoader = useValueNotifier(false);
    final avatarUrl = useValueNotifier('');
    final usernameController = useTextEditingController();
    final phonenumberController = useTextEditingController();
    final emailController = useTextEditingController();
    final accountNumberController = useTextEditingController();
    final bankNameusernameController = useTextEditingController();
    final accountHolderNameController = useTextEditingController();
    final companyNameController = useTextEditingController();
    final representativeNameController = useTextEditingController();
    final companyRegistrationNumberController = useTextEditingController();
    final companyImageController = useTextEditingController();
    final companyAddressController = useTextEditingController();
    final animationController = useAnimationController();
    final passwordController = useTextEditingController();

    final curve =
        CurvedAnimation(parent: animationController, curve: Curves.easeIn);

    bool validate() {
      return radioNotifier.value != null &&
          usernameController.value.text.length > 0 &&
          phonenumberController.value.text.length > 0 &&
          emailController.value.text.length > 0 &&
          accountNumberController.value.text.length > 0 &&
          bankNameusernameController.value.text.length > 0 &&
          accountHolderNameController.value.text.length > 0 &&
          companyNameController.value.text.length > 0 &&
          representativeNameController.value.text.length > 0 &&
          passwordController.value.text.length > 0 &&
          companyRegistrationNumberController.value.text.length > 0;
    }

    radioOnChange(value) {
      radioNotifier.value = value;
    }

    updateMyProfile() async {
      formLoader.value = true;
      var user = {
        'user': {
          "name": usernameController.value.text,
          "email": emailController.value.text,
          "password": passwordController.value.text,
          "bank_name": bankNameusernameController.value.text,
          "bank_account_number": accountNumberController.value.text,
          "account_holder_name": accountHolderNameController.value.text,
          "business_kind": radioNotifier.value == 0
              ? 'private_business'
              : 'corporate_business',
          'avatar_url': avatarUrl.value.length > 0 ? avatarUrl.value : null,
          'phone_number': phonenumberController.value.text,
          'company_name': companyNameController.value.text,
          'representative_name': representativeNameController.value.text,
          'company_registration_number':
              companyRegistrationNumberController.value.text,
          'company_registration_doc_url':
              companyImageController.value.text.length > 0
                  ? companyImageController.value.text
                  : null,
          'office_address': companyAddressController.value.text
        }
      };

      final profileRespose = await signUp(user);
      if (profileRespose['error'] == null) {
        FlutterToast.showSuccess(
            context: context, message: profileRespose['message']);
        formLoader.value = false;

        Navigator.pop(context);
        Routes.sailor.navigate('/');
      } else {
        formLoader.value = false;

        FlutterToast.showErrorToast(
            context: context, message: profileRespose['message']);
      }
    }

    useListenable(radioNotifier);
    useListenable(usernameController);
    useListenable(phonenumberController);
    useListenable(emailController);
    useListenable(accountNumberController);
    useListenable(bankNameusernameController);
    useListenable(accountHolderNameController);
    useListenable(companyNameController);
    useListenable(representativeNameController);
    useListenable(companyRegistrationNumberController);
    useListenable(avatarUrl);
    useListenable(companyImageController);
    useListenable(passwordController);

    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        floatingActionButton: validate()
            ? loginButton(
                context: context,
                onPressed: () async {
                  await updateMyProfile();
                },
                text: 'Signup',
                width: double.maxFinite)
            : loginButtonDisabled(
                context: context,
                onPressed: () {},
                text: 'Signup',
                width: double.maxFinite),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        body: !formLoader.value
            ? SignInBackground(
                child: Stack(
                clipBehavior: Clip.none,
                children: [
                  Positioned(
                    top: 0,
                    child: InkWell(
                      onTap: () async {
                        FlutterImagePicker.imagePickerModalSheet(
                            context: context,
                            fromCamera: () async {
                              final imgFile = await pickImageFromDevice(
                                  context: context, pickerType: 'camera');
                              // uploadImageToS3(
                              //     imgFile, 'profile_image');
                            },
                            fromGallery: () async {
                              final imgFile = await pickImageFromDevice(
                                  context: context, pickerType: 'gallery');
                              // uploadImageToS3(
                              //     imgFile, 'profile_image');
                            });
                      },
                      child: UserProfileHeader(
                        imageUrl: avatarUrl.value != null &&
                                avatarUrl.value.length > 0
                            ? avatarUrl.value
                            : "https://i.ibb.co/V9bBvnB/rods.jpg",
                      ),
                    ),
                  ),
                  Container(
                    height: size.height * .7,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30),
                        )),
                    child: Padding(
                      padding: EdgeInsets.all(20),
                      child: ListView(
                        children: [
                          Column(
                            children: [
                              TextInputWithPrefixSuffixMaterial(
                                  width: size.width,
                                  inputType: TextInputType.text,
                                  borderRadius: 10,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.auto,
                                  hint: 'Username',
                                  controller: usernameController,
                                  validator: (arg) =>
                                      arg.length == 0 ? '성명 필요한' : null),
                              SizedBox(height: 10),
                              TextInputWithPrefixSuffixMaterial(
                                  width: size.width,
                                  inputType: TextInputType.text,
                                  borderRadius: 10,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.auto,
                                  hint: 'Phone number',
                                  controller: phonenumberController,
                                  validator: (arg) =>
                                      arg.length == 0 ? '전화번호 필요한' : null),
                              SizedBox(height: 10),
                              TextInputWithPrefixSuffixMaterial(
                                  width: size.width,
                                  inputType: TextInputType.text,
                                  borderRadius: 10,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.auto,
                                  hint: 'e-mail',
                                  controller: emailController,
                                  validator: (arg) =>
                                      arg.length == 0 ? '이메일 필요한' : null),
                              TextInputWithPrefixSuffixMaterial(
                                  width: size.width,
                                  inputType: TextInputType.text,
                                  borderRadius: 10,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.auto,
                                  hint: 'e-mail',
                                  controller: passwordController,
                                  validator: (arg) =>
                                      arg.length == 0 ? '이메일 필요한' : null),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Radio(
                                    activeColor:
                                        Theme.of(context).primaryColorDark,
                                    value: 0,
                                    groupValue: radioNotifier.value,
                                    onChanged: radioOnChange,
                                  ),
                                  'Private'.styled(inputHint),
                                  SizedBox(width: 10),
                                  Radio(
                                    activeColor:
                                        Theme.of(context).primaryColorDark,
                                    value: 1,
                                    groupValue: radioNotifier.value,
                                    onChanged: radioOnChange,
                                  ),
                                  'Corporate'.styled(inputHint),
                                ],
                              ),
                              SizedBox(height: 10),
                              TextInputWithPrefixSuffixMaterial(
                                  width: size.width,
                                  inputType: TextInputType.text,
                                  borderRadius: 10,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.auto,
                                  hint: 'name of bank',
                                  controller: bankNameusernameController,
                                  validator: (arg) =>
                                      arg.length == 0 ? '은행명 필요한' : null),
                              SizedBox(height: 10),
                              TextInputWithPrefixSuffixMaterial(
                                  width: size.width,
                                  inputType: TextInputType.number,
                                  borderRadius: 10,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.auto,
                                  hint: 'Account Number',
                                  controller: accountNumberController,
                                  validator: (arg) =>
                                      arg.length == 0 ? '계좌번호 필요한' : null),
                              SizedBox(height: 10),
                              TextInputWithPrefixSuffixMaterial(
                                  width: size.width,
                                  inputType: TextInputType.text,
                                  borderRadius: 10,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.auto,
                                  hint: 'Account name',
                                  controller: accountHolderNameController,
                                  validator: (arg) =>
                                      arg.length == 0 ? '계좌명 필요한' : null),
                            ],
                          ),
                          Column(
                            children: [
                              TextInputWithPrefixSuffixMaterial(
                                  width: size.width,
                                  inputType: TextInputType.text,
                                  borderRadius: 10,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.auto,
                                  hint: 'Company Name',
                                  controller: companyNameController,
                                  validator: (arg) =>
                                      arg.length == 0 ? '은행명 필요한' : null),
                              SizedBox(height: 10),
                              TextInputWithPrefixSuffixMaterial(
                                  width: size.width,
                                  inputType: TextInputType.number,
                                  borderRadius: 10,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.auto,
                                  hint: 'Representative name',
                                  controller: representativeNameController,
                                  validator: (arg) =>
                                      arg.length == 0 ? '계좌번호 필요한' : null),
                              SizedBox(height: 10),
                              TextInputWithPrefixSuffixMaterial(
                                  width: size.width,
                                  inputType: TextInputType.text,
                                  borderRadius: 10,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.auto,
                                  hint: 'Company Registration Number',
                                  controller:
                                      companyRegistrationNumberController,
                                  validator: (arg) =>
                                      arg.length == 0 ? '계좌명 필요한' : null),
                              SizedBox(
                                height: 10,
                              ),
                              InkWell(
                                onTap: () async {
                                  FlutterImagePicker.imagePickerModalSheet(
                                      context: context,
                                      fromCamera: () async {
                                        final imgFile =
                                            await pickImageFromDevice(
                                                context: context,
                                                pickerType: 'camera');
                                        // uploadImageToS3(imgFile,
                                        //     'company_profile');
                                      },
                                      fromGallery: () async {
                                        final imgFile =
                                            await pickImageFromDevice(
                                                context: context,
                                                pickerType: 'gallery');
                                        // uploadImageToS3(imgFile,
                                        //     'company_profile');
                                      });
                                },
                                child: TextInputWithPrefixSuffixMaterial(
                                  width: size.width,
                                  enabled: false,
                                  controller: companyImageController,
                                  inputType: TextInputType.text,
                                  borderRadius: 10,
                                  floatingLabelBehavior:
                                      FloatingLabelBehavior.auto,
                                  hint: 'Business registration certificate',
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              TextAreaWithPrefixSuffixMaterial(
                                width: size.width,
                                inputType: TextInputType.text,
                                borderRadius: 10,
                                floatingLabelBehavior:
                                    FloatingLabelBehavior.auto,
                                hint: 'Company Address',
                                controller: companyAddressController,
                              ),
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                ],
              ))
            : Loader());
  }
}
