import 'package:flutter/material.dart';

curvedBottomSheet(
    {@required BuildContext context, @required Widget child, double height}) {
  showModalBottomSheet(
      isScrollControlled: true,
      context: context,
      builder: (BuildContext bc) {
        final size = MediaQuery.of(context).size;
        return SingleChildScrollView(
          child: Container(
            height: height != null ? height : size.height * .5,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20), topRight: Radius.circular(20)),
            ),
            child: child,
          ),
        );
      });
}
