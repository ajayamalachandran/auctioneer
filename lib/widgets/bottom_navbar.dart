import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/routes.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:badges/badges.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';

class CustomBottomBar extends HookWidget {
  final index;
  CustomBottomBar({this.index});

  @override
  Widget build(BuildContext context) {
    final notificationNotifier = useValueNotifier([]);
    useEffect(() {
      getMyNotifications().pipe(notificationNotifier);
    }, []);
    useListenable(notificationNotifier);
    return CurvedNavigationBar(
      backgroundColor: Theme.of(context).backgroundColor,
      color: Theme.of(context).primaryColorDark,
      height: 50,
      index: index,
      animationCurve: Curves.easeIn,
      animationDuration: Duration(milliseconds: 300),
      items: <Widget>[
        Icon(Icons.home_outlined, size: 20, color: Colors.white),
        notificationNotifier.value.length > 0
            ? Stack(
                clipBehavior: Clip.none,
                children: [
                  Icon(Icons.notifications_none_outlined,
                      size: 20, color: Colors.white),
                  Positioned(
                    top: -10,
                    right: -6,
                    child: Badge(
                      badgeContent: Text(
                        '${notificationNotifier.value.length}',
                        style: TextStyle(color: Colors.white, fontSize: 10),
                      ),
                      badgeColor: Theme.of(context).accentColor,
                      padding: EdgeInsets.all(4),
                    ),
                  )
                ],
              )
            : Icon(Icons.notifications_none_outlined,
                size: 20, color: Colors.white),
        Icon(Icons.add, size: 20, color: Colors.white),
        Icon(Icons.person_outline_outlined, size: 20, color: Colors.white),
      ],
      onTap: (index) {
        index == 0
            ? Routes.sailor.navigate('/')
            : index == 2
                ? Routes.sailor.navigate('/new-auction',
                    params: {'kind': 'normal_auction'})
                : index == 1
                    ? Routes.sailor.navigate('/notifications')
                    : Routes.sailor
                        .navigate('/profile', params: {'type': 'userProfile'});
      },
    );
  }
}
