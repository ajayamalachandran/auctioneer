import 'package:collapsible_sidebar/collapsible_sidebar.dart';
import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/core/local-storage.dart';
import 'package:auctioneer/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:layout/layout.dart';

class Sidebar extends HookWidget {
  final Widget child;
  const Sidebar({Key key, this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = useValueNotifier({});

    getUser() async {
      user.value = await getMe();
    }

    useEffect(() {
      getUser();
    }, []);
    useListenable(user);
    return CollapsibleSidebar(
      title: user.value['company_name'] != null
          ? user.value['company_name']
          : user.value['name'] != null
              ? user.value['name']
              : '',
      items: [
        CollapsibleItem(
          text: '대시보드',
          icon: Icons.dashboard,
          onPressed: () => Routes.sailor("/"),
          isSelected: true,
        ),
        CollapsibleItem(
          text: '내 경매/역경매',
          icon: Icons.gavel,
          onPressed: () => Routes.sailor("/my-auction"),
          isSelected: false,
        ),
        CollapsibleItem(
          text: '알림정보',
          icon: Icons.notifications,
          onPressed: () => Routes.sailor.navigate('/notifications'),
          isSelected: false,
        ),
        CollapsibleItem(
          text: '신상정보',
          icon: Icons.person,
          onPressed: () => Routes.sailor("/profile"),
          isSelected: false,
        ),
        CollapsibleItem(
          text: '로그아웃',
          icon: Icons.logout,
          onPressed: () async {
            await SecureStorage.deleteAllSecureData();
            Routes.sailor.navigate('/sign-in');
          },
          isSelected: false,
        )
      ],
      textStyle: TextStyle(fontSize: 20),
      titleStyle: TextStyle(fontSize: 20),
      toggleTitleStyle: TextStyle(fontSize: 20),
      avatarImg: NetworkImage(user.value['avatar_url'] != null
          ? user.value['avatar_url']
          : 'https://i.ibb.co/0KDwYTL/user-icon-male-hipster-avatar-vector-flat-design-user-icon-male-beard-icon-hipster-flat-icon-avatar.jpg'),
      height: double.infinity,
      minWidth: context.layout.value(xs: 70, sm: 75, md: 80),
      maxWidth: 250,
      borderRadius: 5,
      iconSize: context.layout.value(xs: 30, sm: 35, md: 40),
      toggleTitle: '닫기',
      toggleButtonIcon: Icons.chevron_right,
      backgroundColor: Colors.orange[700].withOpacity(0.7),
      selectedIconBox: Color(0xffE5D80D).withOpacity(0.7),
      selectedIconColor: Colors.white,
      selectedTextColor: Color(0xffF3F7F7),
      unselectedIconColor: Colors.grey.shade50,
      unselectedTextColor: Colors.white,
      duration: Duration(milliseconds: 500),
      curve: Curves.fastLinearToSlowEaseIn,
      screenPadding: 4,
      topPadding: 0,
      bottomPadding: 0,
      fitItemsToBottom: true,
      showToggleButton: true,
      body: child,
    );
  }
}
