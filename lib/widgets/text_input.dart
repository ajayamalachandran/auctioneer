import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class TextInputWithPrefixSuffixMaterial extends StatelessWidget {
  TextInputWithPrefixSuffixMaterial(
      {this.hint,
      @required this.inputType,
      this.validator,
      this.onSaved,
      this.key,
      this.focusNode,
      this.onFieldSubmitted,
      this.borderColor,
      this.prefixItem,
      this.suffixItem,
      this.obscureText = false,
      this.backgroundColor,
      this.borderRadius = 0,
      this.controller,
      this.onChange,
      this.maxLine,
      this.onTap,
      this.suffixIcon,
      this.enabled,
      this.autofocus = false,
      this.floatingLabelBehavior,
      this.inputFormatters,
      this.width = 400});

  final Color borderColor;
  final Color backgroundColor;
  final hint;
  final TextInputType inputType;
  final TextEditingController controller;
  final int maxLine;
  final double width;
  final Widget prefixItem;
  final Widget suffixItem;
  final Widget suffixIcon;

  final Function validator;
  final Function onSaved;
  final Function(String) onChange;
  final Function onFieldSubmitted;
  final Function onTap;
  final bool enabled;
  final bool autofocus;

  final Key key;
  final FocusNode focusNode;
  final bool obscureText;
  final double borderRadius;
  final FloatingLabelBehavior floatingLabelBehavior;
  final List<TextInputFormatter> inputFormatters;

  @override
  Widget build(BuildContext context) {
    return Container(
      // height: 60,
      width: width,
      child: TextFormField(
        inputFormatters: inputFormatters,
        controller: controller,
        autofocus: autofocus,
        focusNode: focusNode,
        obscureText: obscureText,
        validator: validator,
        keyboardType: inputType,
        onFieldSubmitted: onFieldSubmitted,
        onChanged: onChange,
        onTap: onTap,
        enabled: enabled,
        textAlign: TextAlign.start,
        decoration: InputDecoration(
            labelText: "$hint",
            floatingLabelBehavior: floatingLabelBehavior != null
                ? floatingLabelBehavior
                : FloatingLabelBehavior.never,
            labelStyle: TextStyle(
              color: Color(0xff1f2a7c).withOpacity(0.4),
              fontSize: 16,
            ),
            prefixIcon: prefixItem,
            suffix: suffixItem,
            suffixIcon: suffixIcon,
            fillColor: backgroundColor != null ? backgroundColor : Colors.white,
            filled: true,
            errorText: null,
            contentPadding:
                EdgeInsets.only(bottom: 20, top: 20, right: 20, left: 10),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.grey[500].withOpacity(0.4), width: 1),
            ),
            disabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.grey[200].withOpacity(0.4), width: 2),
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius),
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.grey, width: 2),
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius),
              ),
            ),
            errorBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xffe58357), width: 3),
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius),
              ),
            ),
            errorStyle: TextStyle(
              color: Color(0xffe58357),
              fontSize: 12,
            )),
      ),
    );
  }
}

class TextAreaWithPrefixSuffixMaterial extends StatelessWidget {
  TextAreaWithPrefixSuffixMaterial(
      {this.hint,
      @required this.inputType,
      this.validator,
      this.onSaved,
      this.key,
      this.focusNode,
      this.onFieldSubmitted,
      this.borderColor,
      this.prefixItem,
      this.suffixItem,
      this.obscureText = false,
      this.floatingLabelBehavior,
      this.backgroundColor,
      this.borderRadius = 0,
      this.controller,
      this.onChange,
      this.maxLine,
      this.onTap,
      this.icon,
      this.width = 400});

  final Color borderColor;
  final Color backgroundColor;
  final hint;
  final TextInputType inputType;
  final TextEditingController controller;
  final int maxLine;
  final Icon icon;

  final Widget prefixItem;
  final Widget suffixItem;

  final Function validator;
  final Function onSaved;
  final Function(String) onChange;
  final Function onFieldSubmitted;
  final Function onTap;

  final Key key;
  final FocusNode focusNode;
  final bool obscureText;
  final double borderRadius;
  final FloatingLabelBehavior floatingLabelBehavior;
  final double width;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      child: TextFormField(
        controller: controller,
        focusNode: focusNode,
        obscureText: obscureText,
        validator: validator,
        keyboardType: TextInputType.multiline,
        onFieldSubmitted: onFieldSubmitted,
        onChanged: onChange,
        onTap: onTap,
        textAlign: TextAlign.left,
        maxLines: 5,
        textAlignVertical: TextAlignVertical.top,
        decoration: InputDecoration(
            floatingLabelBehavior: floatingLabelBehavior,
            icon: icon,
            alignLabelWithHint: true,
            labelText: "$hint",
            labelStyle: TextStyle(
              color: Color(0xff1f2a7c).withOpacity(0.4),
              fontSize: 14,
            ),
            suffix: suffixItem,
            fillColor: backgroundColor != null ? backgroundColor : Colors.white,
            filled: true,
            focusColor: Colors.red,
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.white, width: 1),
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius),
              ),
            ),
            errorText: null,
            contentPadding:
                EdgeInsets.only(left: 10, bottom: 10, top: 10, right: 20),
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.grey, width: 2),
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius),
              ),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.grey, width: 2),
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius),
              ),
            ),
            errorBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Color(0xffe58357), width: 3),
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius),
              ),
            ),
            errorStyle: TextStyle(
              color: Color(0xffe58357),
              fontSize: 12,
            )),
      ),
    );
  }
}

class TextInputSearchMaterial extends StatelessWidget {
  TextInputSearchMaterial(
      {this.hint,
      @required this.inputType,
      this.validator,
      this.onSaved,
      this.key,
      this.focusNode,
      this.onFieldSubmitted,
      this.borderColor,
      this.prefixItem,
      this.suffixItem,
      this.obscureText = false,
      this.backgroundColor,
      this.borderRadius = 0,
      this.controller,
      this.onChange,
      this.maxLine,
      this.onTap,
      this.suffixIcon,
      this.enabled,
      this.autofocus = false,
      this.floatingLabelBehavior,
      this.inputFormatters,
      this.width = 400});

  final Color borderColor;
  final Color backgroundColor;
  final hint;
  final TextInputType inputType;
  final TextEditingController controller;
  final int maxLine;
  final double width;
  final Widget prefixItem;
  final Widget suffixItem;
  final Widget suffixIcon;

  final Function validator;
  final Function onSaved;
  final Function(String) onChange;
  final Function onFieldSubmitted;
  final Function onTap;
  final bool enabled;
  final bool autofocus;

  final Key key;
  final FocusNode focusNode;
  final bool obscureText;
  final double borderRadius;
  final FloatingLabelBehavior floatingLabelBehavior;
  final List<TextInputFormatter> inputFormatters;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: width,
      child: TextFormField(
        inputFormatters: inputFormatters,
        controller: controller,
        autofocus: autofocus,
        focusNode: focusNode,
        obscureText: obscureText,
        validator: validator,
        keyboardType: inputType,
        onFieldSubmitted: onFieldSubmitted,
        onChanged: onChange,
        onTap: onTap,
        enabled: enabled,
        textAlign: TextAlign.start,
        decoration: InputDecoration(
            labelText: "$hint",
            floatingLabelBehavior: floatingLabelBehavior != null
                ? floatingLabelBehavior
                : FloatingLabelBehavior.never,
            labelStyle: TextStyle(
              color: Color(0xff1f2a7c).withOpacity(0.4),
              fontSize: 16,
            ),
            prefixIcon: prefixItem,
            suffix: suffixItem,
            suffixIcon: suffixIcon,
            fillColor: backgroundColor != null ? backgroundColor : Colors.white,
            filled: true,
            errorText: null,
            contentPadding:
                EdgeInsets.only(bottom: 20, top: 20, right: 20, left: 10),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.grey[500].withOpacity(0.4), width: 1),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.grey[200].withOpacity(0.4), width: 2),
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius),
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.grey, width: 1),
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius),
              ),
            ),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Color(0xffe58357), width: 3),
              borderRadius: BorderRadius.all(
                Radius.circular(borderRadius),
              ),
            ),
            errorStyle: TextStyle(
              color: Color(0xffe58357),
              fontSize: 12,
            )),
      ),
    );
  }
}
