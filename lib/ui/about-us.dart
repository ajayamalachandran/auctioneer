import 'package:auctioneer/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';

class AboutUs extends HookWidget {
  const AboutUs({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      "회사소개".styled(howItWorksTitleStyle),
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage("assets/about-us.jpg"),
          ),
        ),
      ).expandFlex(1),
    ]);
  }
}
