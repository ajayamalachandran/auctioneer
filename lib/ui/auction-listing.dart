import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/ui/auction-card.dart';
import 'package:auctioneer/ui/auction-tile.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class AuctionListing extends HookWidget {
  final type, query;
  final bool isList;
  final ValueNotifier<String> searchQuery;

  const AuctionListing(
      {Key key, this.type, this.query, this.searchQuery, this.isList})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final auctionNotifier = useValueNotifier([]);

    getAuctionsList() async {
      final auctionsResponse =
          await getAuctions(type, query, searchQuery.value);
      auctionNotifier.value = auctionsResponse;
    }

    useEffect(() {
      if (searchQuery == null) return;
      searchQuery.addListener(() async {
        getAuctionsList();
      });
    }, []);

    useEffect(() {
      getAuctionsList();
    }, []);

    useListenable(auctionNotifier);
    return auctionNotifier.value != null && auctionNotifier.value.length > 0
        ? Container(
            alignment: Alignment.center,
            padding: EdgeInsets.symmetric(vertical: 0, horizontal: 20),
            child: Wrap(
              children: auctionNotifier.value.map((auction) {
                return isList
                    ? AuctionTile(
                        link: auction['winning_tx_link'],
                        type: type,
                        kind: auction['bid_kind'],
                        unitsAvailable: auction['volume'],
                        name: auction['name'],
                        price: auction['unit_price'],
                        imageUrl: auction['preview_urls'] != null &&
                                auction['preview_urls'].length > 0
                            ? auction['preview_urls'][0]
                            : "https://i.ibb.co/V9bBvnB/rods.jpg",
                        auctionInfo: auction)
                    : AuctionCard(
                        link: auction['winning_tx_link'],
                        type: type,
                        kind: auction['bid_kind'],
                        unitsAvailable: auction['volume'],
                        name: auction['name'],
                        price: auction['unit_price'],
                        imageUrl: auction['preview_urls'] != null &&
                                auction['preview_urls'].length > 0
                            ? auction['preview_urls'][0]
                            : "https://i.ibb.co/V9bBvnB/rods.jpg",
                        auctionInfo: auction);
              }).toList(),
            ),
          )
        : Container();
  }
}
