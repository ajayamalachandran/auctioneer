import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:outline_search_bar/outline_search_bar.dart';
import 'package:layout/layout.dart';

class AuctionSearchBar extends HookWidget {
  final ValueNotifier<String> searchQuery;
  const AuctionSearchBar({Key key, this.searchQuery}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: context.layout.value(xs: 300),
      child: OutlineSearchBar(
        onSearchButtonPressed: (searchText) {
          searchQuery.value = searchText;
        },
        borderRadius: BorderRadius.circular(10),
        elevation: 4,
        hintText: "Search",
      ),
    );
  }
}
