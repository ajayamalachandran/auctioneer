import 'package:auctioneer/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';

class Intro extends HookWidget {
  const Intro({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      final items = [
        Image(
          alignment: Alignment.center,
          image: AssetImage("assets/public-auction.png"),
        ),
        Align(
          alignment: Alignment.center,
          child:
              "Welcome to\nThe world of Metal\nAuctions On Blockchain".styled(
            constraints.maxWidth > 1024
                ? introTitleStyle
                : introTitleStyleSmall,
          ),
        )
      ];
      return Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          fit: BoxFit.cover,
          image: AssetImage('assets/intro-bg.jpg'),
        )),
        alignment: Alignment.center,
        child: constraints.maxWidth > 1024
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: items,
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: items),
      );
    });
  }
}
