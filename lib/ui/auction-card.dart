import 'package:auctioneer/routes.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:auctioneer/custom.dart';
import 'package:auctioneer/hooks/use-styles.dart';
import 'package:auctioneer/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';
import 'package:layout/layout.dart';
import 'package:sliding_card/sliding_card.dart';
import 'package:auctioneer/extensions.dart';

class AuctionCard extends HookWidget {
  final String kind, imageUrl, name, price, unitsAvailable, link;
  final auctionInfo, type;
  const AuctionCard(
      {Key key,
      this.unitsAvailable = "180",
      this.name = "Hard Fibre Metal Pipes",
      this.price = "2,35,000",
      this.kind = "Buying",
      this.auctionInfo,
      this.type,
      this.link,
      this.imageUrl = "https://i.ibb.co/WtmBb70/metal-tubes.jpg"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formatter = NumberFormat('###,###,###,###,000.00');
    final formatterNonDecimal = NumberFormat('###,###,###,###,000');

    final styleHook = useStyles(context);
    final cardController = useState(SlidingCardController());
    final bidPrice = useValueNotifier(MoneyMaskedTextController(
        decimalSeparator: '.', thousandSeparator: ','));
    final totalPriceNotifier = useValueNotifier('0');
    int endTime =
        DateTime.parse(auctionInfo['end_time']).millisecondsSinceEpoch +
            1000 * 30;

    String getTotalLabelValue() {
      return (double.parse(unitsAvailable) *
              double.parse(bidPrice.value.numberValue > 0
                  ? bidPrice.value.numberValue.toString()
                  : '0'))
          .toString();
    }

    bool isAuctionEnded() {
      return DateTime.now().millisecondsSinceEpoch >
          DateTime.parse(auctionInfo['end_time']).millisecondsSinceEpoch;
    }

    useListenable(totalPriceNotifier);
    useListenable(bidPrice.value);
    return InkWell(
      onTap: () {
        Routes.sailor
            .navigate('/auction-details', params: {'auction': auctionInfo});
      },
      child: LayoutBuilder(builder: (context, constraints) {
        final boxDecoration = BoxDecoration(
          boxShadow: cardBoxShadow,
          borderRadius: BorderRadius.circular(10),
          color: Theme.of(context).cardColor,
        );
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Container(
              decoration: boxDecoration,
              child: Wrap(
                runSpacing: 10,
                spacing: 10,
                direction: context.breakpoint > LayoutBreakpoint.sm
                    ? Axis.vertical
                    : Axis.horizontal,
                alignment: context.layout
                    .value(xs: WrapAlignment.start, md: WrapAlignment.start),
                runAlignment: context.breakpoint > LayoutBreakpoint.sm
                    ? WrapAlignment.start
                    : WrapAlignment.center,
                crossAxisAlignment: WrapCrossAlignment.center,
                children: [
                  CachedNetworkImage(
                    imageUrl: imageUrl,
                    imageBuilder: (context, imageProvider) => Container(
                      height: 180,
                      padding: EdgeInsets.symmetric(vertical: 20),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                        image: DecorationImage(
                            image: imageProvider, fit: BoxFit.cover),
                      ),
                      child: Stack(clipBehavior: Clip.none, children: [
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Align(
                            alignment: Alignment.topRight,
                            child: Container(
                              padding: EdgeInsets.all(5),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Theme.of(context).backgroundColor),
                              child: Icon(Icons.favorite_border_outlined),
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: -35,
                          right: 0,
                          child: Row(
                            children: [
                              Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 5, vertical: 10),
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                  color: Theme.of(context).backgroundColor,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      bottomLeft: Radius.circular(10)),
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.black.withOpacity(0.2),
                                      blurRadius: 6.0,
                                      spreadRadius: 2.0,
                                      offset: Offset(
                                        5.0,
                                        5.0,
                                      ),
                                    )
                                  ],
                                ),
                                child: CountdownTimer(
                                  endTime: endTime,
                                  widgetBuilder:
                                      (_, CurrentRemainingTime time) {
                                    if (time == null) {
                                      return auctionCountdown(
                                          context: context,
                                          icon: Icons.timer,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                          label: " Finished");
                                    } else {
                                      return auctionCountdown(
                                          context: context,
                                          icon: Icons.timer,
                                          color: Theme.of(context)
                                              .primaryColorDark,
                                          label:
                                              " ${time.days != null ? time.days : 0}d ${time.hours != null ? time.hours : 0}h ${time.min != null ? time.min : 0}m ${time.sec != null ? time.sec : 0}s");
                                    }
                                  },
                                ),
                              ),
                              Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 10, vertical: 10),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                    color: Theme.of(context).primaryColorDark,
                                  ),
                                  child: '\$ 50'.styled(auctionCardPrice)),
                            ],
                          ),
                        ),
                      ]),
                    ),
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    child: Wrap(
                      direction: Axis.vertical,
                      crossAxisAlignment: WrapCrossAlignment.start,
                      alignment: WrapAlignment.start,
                      runAlignment: WrapAlignment.start,
                      spacing: 10,
                      children: [
                        Container(
                          width: 260,
                          padding: EdgeInsets.symmetric(vertical: 0),
                          child: name.styled(auctionTitleStyle(context)),
                        ),
                        Container(
                          width: 260,
                          child:
                              'Made from Highest quality food grade stainless steel and Mirror finish'
                                  .styled(auctionDescriptionStyle),
                        ),
                      ],
                    ),
                  ),
                ],
              )),
        );
      }),
    );
  }
}
