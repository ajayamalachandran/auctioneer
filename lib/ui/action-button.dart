import 'package:auctioneer/routes.dart';
import 'package:flutter/material.dart';
import 'package:hawk_fab_menu/hawk_fab_menu.dart';

class ActionButton extends StatelessWidget {
  const ActionButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return HawkFabMenu(
      icon: AnimatedIcons.menu_close,
      fabColor: Colors.orange[700],
      iconColor: Colors.white,
      items: [
        HawkFabMenuItem(
          label: '경매 생성',
          ontap: () {
            Routes.sailor.navigate<bool>("/new-auction", params: {
              'kind': 'normal_auction',
            });
          },
          icon: Icon(
            Icons.gavel_rounded,
            color: Colors.orange[700],
          ),
          color: Colors.white,
          labelColor: Colors.orange[700],
        ),
        HawkFabMenuItem(
          label: '역경매 생성',
          ontap: () {
            Routes.sailor.navigate<bool>("/new-auction", params: {
              'kind': 'reverse_auction',
            });
          },
          icon: Icon(
            Icons.gavel_rounded,
            color: Colors.orange[700],
          ),
          color: Colors.white,
          labelColor: Colors.orange[700],
        ),
      ],
      body: Container(),
    );
  }
}
