import 'package:auctioneer/canvas/auction-tile-canvas-left.dart';
import 'package:auctioneer/canvas/auction-tile-canvas-right.dart';
import 'package:auctioneer/routes.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:auctioneer/custom.dart';
import 'package:auctioneer/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:auctioneer/extensions.dart';

class AuctionTile extends HookWidget {
  final String kind, imageUrl, name, price, unitsAvailable, link;
  final auctionInfo, type;
  const AuctionTile(
      {Key key,
      this.unitsAvailable = "180",
      this.name = "Hard Fibre Metal Pipes",
      this.price = "2,35,000",
      this.kind = "Buying",
      this.auctionInfo,
      this.type,
      this.link,
      this.imageUrl = "https://i.ibb.co/WtmBb70/metal-tubes.jpg"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bidPrice = useValueNotifier(MoneyMaskedTextController(
        decimalSeparator: '.', thousandSeparator: ','));
    final totalPriceNotifier = useValueNotifier('0');
    int endTime =
        DateTime.parse(auctionInfo['end_time']).millisecondsSinceEpoch +
            1000 * 30;
    final size = MediaQuery.of(context).size;

    String getTotalLabelValue() {
      return (double.parse(unitsAvailable) *
              double.parse(bidPrice.value.numberValue > 0
                  ? bidPrice.value.numberValue.toString()
                  : '0'))
          .toString();
    }

    bool isAuctionEnded() {
      return DateTime.now().millisecondsSinceEpoch >
          DateTime.parse(auctionInfo['end_time']).millisecondsSinceEpoch;
    }

    useListenable(totalPriceNotifier);
    useListenable(bidPrice.value);
    return InkWell(
      onTap: () {
        Routes.sailor
            .navigate('/auction-details', params: {'auction': auctionInfo});
      },
      child: LayoutBuilder(builder: (context, constraints) {
        final boxDecoration = BoxDecoration(
            color: Theme.of(context).cardColor,
            border: Border(
                bottom:
                    BorderSide(color: Colors.grey.withOpacity(0.2), width: 1)));
        return Container(
          decoration: boxDecoration,
          child: Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Row(
                      children: [
                        CachedNetworkImage(
                          imageUrl: imageUrl,
                          imageBuilder: (context, imageProvider) => Container(
                            height: 30,
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: imageProvider, fit: BoxFit.cover),
                            ),
                          ),
                          placeholder: (context, url) =>
                              CircularProgressIndicator(),
                          errorWidget: (context, url, error) =>
                              Icon(Icons.error),
                        ),
                        Padding(
                          padding: EdgeInsets.symmetric(
                              horizontal: 20, vertical: 10),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                width: 160,
                                padding: EdgeInsets.symmetric(vertical: 0),
                                child: name.styled(auctionTileTitleText(context)
                                  ..textOverflow(TextOverflow.ellipsis)),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              CountdownTimer(
                                endTime: endTime,
                                widgetBuilder: (_, CurrentRemainingTime time) {
                                  if (time == null) {
                                    return 'Expired'.styled(
                                        auctionCountDownTileStyle(context));
                                  } else {
                                    return " ${time.days != null ? time.days : 0}d ${time.hours != null ? time.hours : 0}h ${time.min != null ? time.min : 0}m ${time.sec != null ? time.sec : 0}s"
                                        .styled(
                                            auctionCountDownTileStyle(context));
                                  }
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColorDark,
                    ),
                    child: '\$ 50'.styled(auctionCardPrice),
                  ),
                ],
              ),
            ],
          ),
        );
      }),
    );
  }
}
