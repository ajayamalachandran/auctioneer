import 'package:auctioneer/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';

class ContactUs extends HookWidget {
  const ContactUs({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      "Contact us".styled(howItWorksTitleStyle),
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage("assets/contact-us.jpg"),
          ),
        ),
      ).expandFlex(1),
    ]);
  }
}
