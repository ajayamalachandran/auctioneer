import 'package:auctioneer/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';

class HowItWorks extends HookWidget {
  const HowItWorks({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      "How it works".styled(howItWorksTitleStyle),
      Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            fit: BoxFit.fill,
            image: AssetImage("assets/infographic-steps.jpg"),
          ),
        ),
      ).expandFlex(1),
    ]);
  }
}
