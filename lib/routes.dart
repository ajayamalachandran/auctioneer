import 'package:auctioneer/core/local-storage.dart';
import 'package:auctioneer/pages/auction-details.dart';
import 'package:auctioneer/pages/dashboard.dart';
import 'package:auctioneer/pages/edit-profile.dart';
import 'package:auctioneer/pages/forgot-password.dart';
import 'package:auctioneer/pages/landing.dart';
import 'package:auctioneer/pages/my-auctions.dart';
import 'package:auctioneer/pages/new-auction.dart';
import 'package:auctioneer/pages/notification.dart';
import 'package:auctioneer/pages/reset-password.dart';
import 'package:auctioneer/pages/settings.dart';
import 'package:auctioneer/pages/sign-in.dart';
import 'package:auctioneer/pages/signup.dart';
import 'package:auctioneer/pages/splash.dart';
import 'package:auctioneer/pages/user_profile.dart';
import 'package:auctioneer/ui/contact-us.dart';
import 'package:flutter/material.dart';
import 'package:sailor/sailor.dart';

class Routes {
  static final sailor = Sailor(
      options: SailorOptions(
    defaultTransitions: [
      SailorTransition.slide_from_bottom,
    ],
    defaultTransitionCurve: Curves.decelerate,
    defaultTransitionDuration: Duration(milliseconds: 500),
  ));

  static void createRoutes() {
    // on-boarding

    sailor.addRoute(SailorRoute(
      name: "/splash",
      builder: (context, args, params) {
        return Splash();
      },
    ));
    sailor.addRoute(SailorRoute(
      name: "/root",
      builder: (context, args, params) {
        return Landing();
      },
      routeGuards: [
        SailorRouteGuard.simple((context, args, params) async {
          // Can open logic goes here.
          if (await SecureStorage.getToken() != null) {
            return true;
          }
          return false;
        }),
      ],
    ));

    sailor.addRoute(SailorRoute(
      name: "/edit-profile",
      builder: (context, args, params) {
        return EditProfile();
      },
      routeGuards: [
        SailorRouteGuard.simple((context, args, params) async {
          if (await SecureStorage.getToken() != null) {
            return true;
          } else {
            return Routes.sailor.navigate('/sign-in');
          }
        }),
      ],
    ));

    sailor.addRoute(SailorRoute(
      name: "/profile",
      builder: (context, args, params) {
        return UserProfile();
      },
      routeGuards: [
        SailorRouteGuard.simple((context, args, params) async {
          if (await SecureStorage.getToken() != null) {
            return true;
          } else {
            return Routes.sailor.navigate('/sign-in');
          }
        }),
      ],
    ));

    sailor.addRoute(SailorRoute(
      name: "/notifications",
      builder: (context, args, params) {
        return NotificationPage();
      },
      routeGuards: [
        SailorRouteGuard.simple((context, args, params) async {
          if (await SecureStorage.getToken() != null) {
            return true;
          } else {
            return Routes.sailor.navigate('/sign-in');
          }
        }),
      ],
    ));

    sailor.addRoute(SailorRoute(
      name: "/",
      builder: (context, args, params) {
        return Dashboard();
      },
      routeGuards: [
        SailorRouteGuard.simple((context, args, params) async {
          if (await SecureStorage.getToken() != null) {
            return true;
          } else {
            return Routes.sailor.navigate('/sign-in');
          }
        }),
      ],
    ));
    sailor.addRoute(
      SailorRoute(
        name: "/new-auction",
        params: [
          SailorParam<String>(
            name: 'kind',
            defaultValue: 'normal_auction',
          ),
        ],
        builder: (context, args, params) {
          return NewAution(kind: params.param<String>("kind"));
        },
        routeGuards: [
          SailorRouteGuard.simple((context, args, params) async {
            if (await SecureStorage.getToken() != null) {
              return true;
            } else {
              return Routes.sailor.navigate('/sign-in');
            }
          }),
        ],
      ),
    );

    sailor.addRoute(
      SailorRoute(
        name: "/my-auction",
        builder: (context, args, params) {
          return MyAuctions();
        },
        routeGuards: [
          SailorRouteGuard.simple((context, args, params) async {
            if (await SecureStorage.getToken() != null) {
              return true;
            } else {
              return Routes.sailor.navigate('/sign-in');
            }
          }),
        ],
      ),
    );

    sailor.addRoute(
      SailorRoute(
        name: "/auction-details",
        builder: (context, args, params) {
          return AuctionDetails(
            auction: params.param<dynamic>('auction'),
          );
        },
        params: [
          SailorParam<String>(
            name: 'auction',
          ),
        ],
        routeGuards: [
          SailorRouteGuard.simple((context, args, params) async {
            if (await SecureStorage.getToken() != null) {
              return true;
            } else {
              return Routes.sailor.navigate('/sign-in');
            }
          }),
        ],
      ),
    );

    sailor.addRoute(
      SailorRoute(
        name: "/settings",
        builder: (context, args, params) {
          return Settings();
        },
        routeGuards: [
          SailorRouteGuard.simple((context, args, params) async {
            if (await SecureStorage.getToken() != null) {
              return true;
            } else {
              return Routes.sailor.navigate('/sign-in');
            }
          }),
        ],
      ),
    );

    sailor.addRoute(
      SailorRoute(
        name: "/sign-in",
        builder: (context, args, params) {
          return SignIn();
        },
      ),
    );

    sailor.addRoute(
      SailorRoute(
        name: "/forget-password",
        builder: (context, args, params) {
          return ForgotPassword();
        },
      ),
    );
    sailor.addRoute(
      SailorRoute(
        name: "/reset-password",
        builder: (context, args, params) {
          return ResetPassword();
        },
      ),
    );
    sailor.addRoute(
      SailorRoute(
        name: "/sign-up",
        builder: (context, args, params) {
          return SignUp();
        },
      ),
    );
    sailor.addRoute(
      SailorRoute(
        name: "/contact-us",
        builder: (context, args, params) {
          return ContactUs();
        },
      ),
    );
  }
}
