import 'package:auctioneer/core/dark_theme/dark_theme_provider.dart';
import 'package:auctioneer/routes.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:layout/layout.dart';
import 'package:provider/provider.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.

  print("Handling a background message: ${message.messageId}");
}

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await FirebaseMessaging.instance.getToken();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  Routes.createRoutes();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  DarkThemeProvider themeChangeProvider = new DarkThemeProvider();
  FirebaseMessaging messaging = FirebaseMessaging.instance;

  @override
  void initState() {
    super.initState();
    getCurrentAppTheme();
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      print(notification.title);
      print(message.data);
    });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      print(notification);
      Routes.sailor.navigate('/notifications');
    });
  }

  void getCurrentAppTheme() async {
    themeChangeProvider.darkTheme =
        await themeChangeProvider.darkThemePreference.getTheme();
  }

  Widget build(BuildContext context) {
    return ChangeNotifierProvider(create: (_) {
      return themeChangeProvider;
    }, child: Consumer<DarkThemeProvider>(
      builder: (BuildContext context, value, Widget child) {
        return Layout(
          child: MaterialApp(
            title: 'Auctioneer',
            debugShowCheckedModeBanner: false,
            navigatorKey: Routes.sailor.navigatorKey,
            onGenerateRoute: Routes.sailor.generator(),
            themeMode: themeChangeProvider.darkTheme
                ? ThemeMode.dark
                : ThemeMode.system,
            darkTheme: ThemeData(
                brightness: Brightness.dark,
                canvasColor: Colors.transparent,
                cardColor: Color(0xff1f1f1f),
                accentColor: Color(0xfff9b930),
                primaryColorDark: Color(0xffd0d0d0),
                backgroundColor: Color(0xff121212),
                bottomAppBarColor: Colors.transparent,
                bottomSheetTheme: BottomSheetThemeData(
                    backgroundColor: Colors.black.withOpacity(0)),
                primarySwatch: Colors.blue,
                visualDensity: VisualDensity.adaptivePlatformDensity,
                disabledColor: Color(0xff1f1f1f),
                fontFamily: 'Quicksand'),
            theme: ThemeData(
                canvasColor: Colors.transparent,
                accentColor: Color(0xfff9b930),
                cardColor: Colors.white,
                primaryColorDark: Color(0xff0c204f),
                backgroundColor: Color(0xfff8f8fb),
                bottomAppBarColor: Colors.transparent,
                bottomSheetTheme: BottomSheetThemeData(
                    backgroundColor: Colors.black.withOpacity(0)),
                primarySwatch: Colors.blue,
                visualDensity: VisualDensity.adaptivePlatformDensity,
                disabledColor: Colors.grey,
                fontFamily: 'Quicksand'),
            initialRoute: '/splash',
          ),
        );
      },
    ));
  }
}
