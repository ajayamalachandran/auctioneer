import 'dart:io';
import 'package:auctioneer/core/tools/Image_picker_methods.dart';
import 'package:flutter/material.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/extensions.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:image_cropper/image_cropper.dart';

final menuIcon = Icon(
  Icons.menu,
  color: Colors.white,
);
final cardBoxShadow = [
  BoxShadow(
      color: Colors.grey.shade400.withOpacity(0.2),
      spreadRadius: 1,
      blurRadius: 5.0,
      offset: Offset(0.0, 0.7)),
];

final iconLabel = (
        {icon = Icons.gavel_outlined,
        String label = "buy",
        color = Colors.green}) =>
    Wrap(
      spacing: 5,
      children: [Icon(icon, color: color), label.styled(auctionSubStyle)],
    );

final auctionCountdown = (
        {context,
        icon = Icons.gavel_outlined,
        String label = "buy",
        color = Colors.green}) =>
    Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Icon(
          icon,
          color: color,
          size: 14,
        ),
        label.styled(auctionCountDownStyle(context)..textColor(color))
      ],
    );

final iconLabelNotification = (
        {icon = Icons.gavel_outlined,
        String label = "Buying",
        color = Colors.green}) =>
    Wrap(
      spacing: 5,
      direction: Axis.horizontal,
      alignment: WrapAlignment.center,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: [
        Icon(
          icon,
          color: color,
          size: 14,
        ),
        label.styled(notificationTime)
      ],
    );
final loginButton =
    ({context, onPressed, String text, double width}) => Container(
          height: 48,
          child: ButtonTheme(
            minWidth: width != null ? width : double.maxFinite,
            child: FlatButton(
              onPressed: onPressed,
              color: Theme.of(context).accentColor,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              textColor: Color(0xff45477C),
              child: text.styled(authBtnText),
            ),
          ),
        );
final bottomBarBtn =
    ({context, onPressed, String text, double width}) => Container(
          height: 48,
          child: ButtonTheme(
            minWidth: width != null ? width : double.maxFinite,
            child: FlatButton(
              onPressed: onPressed,
              color: Theme.of(context).primaryColorDark,
              textColor: Color(0xff45477C),
              child: text.styled(authBtnText),
            ),
          ),
        );

final loginButtonDisabled =
    ({context, onPressed, String text, double width}) => Container(
          height: 48,
          child: ButtonTheme(
            minWidth: width != null ? width : double.maxFinite,
            child: FlatButton(
              onPressed: onPressed,
              color: Colors.grey[300],
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
              ),
              textColor: Color(0xff45477C),
              child: text.styled(authBtnText),
            ),
          ),
        );

final expandedButton = ({
  onTap,
  Icon icon,
}) =>
    InkWell(
      hoverColor: Colors.grey,
      splashColor: Colors.white,
      onTap: onTap,
      child: Container(
        alignment: Alignment.center,
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: Colors.black,
        ),
        child: Align(alignment: Alignment.center, child: icon),
      ),
    );

final iconButton =
    ({onPressed, Icon icon, Color color, double width}) => Center(
            child: Ink(
          decoration: ShapeDecoration(
            color: color,
            shape: CircleBorder(),
          ),
          child: IconButton(
            icon: const Icon(Icons.android),
            color: Colors.white,
            onPressed: () {},
          ),
        ));
final profileContainer =
    ({List<Widget> children, CrossAxisAlignment crossAxisAlignment}) => Align(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: crossAxisAlignment != null
              ? crossAxisAlignment
              : CrossAxisAlignment.start,
          children: children,
        ));

final kindTile = (text, {Color color}) => Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
      decoration:
          BoxDecoration(borderRadius: BorderRadius.circular(5), color: color),
      child: "$text".styled(kindText),
    );

final kindTilePopup = (text, {Color color}) => Container(
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
      decoration:
          BoxDecoration(borderRadius: BorderRadius.circular(5), color: color),
      child: "$text".styled(kindTextPopup),
    );

final expandTimerColumn = ({List<Widget> children}) => Padding(
    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
    child: Column(
      children: children,
    ));

final emptyIcon = SvgPicture.asset(
  'assets/empty.svg',
  color: Colors.orange[700],
  height: 40,
);

final noDataFound = (String text) => Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          emptyIcon,
          SizedBox(height: 20),
          'Uh-oh!'.styled(noDataFoundText),
          SizedBox(height: 4),
          'Sorry there’s no $text here'.styled(noDataFoundText),
        ],
      ),
    );
final googleNewIcon = SvgPicture.asset(
  'assets/svg/google_color.svg',
  height: 40,
  width: 40,
);

final fbNewIcon = SvgPicture.asset(
  'assets/svg/fb_color.svg',
  height: 40,
  width: 40,
);

final settingsIcon = (context, IconData icon) => Icon(
      icon,
      size: 20,
      color: Theme.of(context).primaryColorDark,
    );
final settingsRightArrowIcon = (context) => Align(
        child: Icon(
      Icons.arrow_forward_ios_outlined,
      size: 20,
      color: Theme.of(context).primaryColorDark,
    ));

final floatingActionButtonEnabled = (
        {onTap,
        @required double width,
        @required String text,
        @required BuildContext context}) =>
    InkWell(
      onTap: onTap,
      child: Container(
        width: width,
        padding: EdgeInsets.symmetric(vertical: 20),
        decoration: BoxDecoration(
          color: Theme.of(context).primaryColorDark,
          borderRadius: BorderRadius.circular(30),
        ),
        child: Text(
          "$text",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ),
    );

final floatingActionButtonDisabled = (
        {onTap,
        @required double width,
        @required String text,
        @required BuildContext context}) =>
    InkWell(
      onTap: onTap,
      child: Container(
        width: width,
        padding: EdgeInsets.symmetric(vertical: 20),
        decoration: BoxDecoration(
          color: Theme.of(context).disabledColor,
          borderRadius: BorderRadius.circular(30),
        ),
        child: Text(
          "$text",
          textAlign: TextAlign.center,
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20),
        ),
      ),
    );

final commonAppBar = (context, String text) => AppBar(
      leading: InkWell(
        onTap: () {
          Navigator.pop(context);
        },
        child: Align(
            alignment: Alignment.center,
            child: Icon(
              Icons.navigate_before,
              color: Theme.of(context).primaryColorDark,
            )),
      ),
      elevation: 0,
      centerTitle: true,
      title: text.styled(commonAppbarText(context)),
      backgroundColor: Colors.transparent,
    );

final appBarWithActions = (
        {@required context,
        @required String text,
        @required List<Widget> actions}) =>
    AppBar(
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Align(
              alignment: Alignment.center,
              child: Icon(
                Icons.navigate_before,
                color: Theme.of(context).primaryColorDark,
              )),
        ),
        elevation: 0,
        centerTitle: false,
        title: text.styled(commonAppbarText(context)),
        backgroundColor: Colors.transparent,
        actions: actions);

Future<File> pickImageFromDevice({context, pickerType}) async {
  final t = pickerType == 'gallery'
      ? await FlutterImagePicker.getImageGalleryWithoutPop(context)
      : await FlutterImagePicker.getImageCameraWIthoutPop(context);
  final imgFile = await ImageCropper.cropImage(
      sourcePath: t.path,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
      ],
      androidUiSettings: AndroidUiSettings(
          toolbarTitle: 'Cropper',
          toolbarColor: Colors.deepOrange,
          toolbarWidgetColor: Colors.white,
          initAspectRatio: CropAspectRatioPreset.square,
          lockAspectRatio: false),
      iosUiSettings: IOSUiSettings(
        minimumAspectRatio: 1.0,
      ));
  return imgFile;
}

Future<File> imageCompression(file, String targetPath) async {
  var result = await FlutterImageCompress.compressAndGetFile(
    file.absolute.path,
    targetPath,
    quality: 50,
  );
  return result;
}
