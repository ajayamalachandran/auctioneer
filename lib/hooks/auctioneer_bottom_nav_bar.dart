import 'package:auctioneer/widgets/bottom_navbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

ValueNotifier<BottomNav> useAuctioneerBottomNavBar(
    BuildContext context, index) {
  final showNav = useState(false);
  final notifier = useState(BottomNav(bottomBar: null));
  useEffect(() {
    final bottomBar = CustomBottomBar(
      index: index,
    );
    notifier.value = BottomNav(bottomBar: bottomBar);
  }, [showNav.value]);

  return notifier;
}

class BottomNav {
  final Widget bottomBar;
  BottomNav({
    @required this.bottomBar,
  });
}
