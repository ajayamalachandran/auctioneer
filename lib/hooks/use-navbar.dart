import 'package:cached_network_image/cached_network_image.dart';
import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/pages/contact-us.dart';
import 'package:auctioneer/routes.dart';
import 'package:auctioneer/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';
import 'package:sailor/sailor.dart';
import 'package:url_launcher/url_launcher.dart';

NavbarHook useNavbar() {
  final pageController = usePageController();
  final user = useValueNotifier({});

  getUser() async {
    user.value = await getMe();
  }

  useEffect(() {
    getUser();
  }, []);
  useListenable(user);
  final appBar = CustomAppBar(
    pageController: pageController,
    user: user.value,
  );

  return NavbarHook(appBar: appBar, pageController: pageController);
}

class CustomAppBar extends HookWidget implements PreferredSizeWidget {
  get preferredSize => Size.fromHeight(80);

  get child => Hero(
        tag: "navbar",
        child: LayoutBuilder(
          builder: (context, constraints) => AppBar(
            toolbarHeight: 80,
            backgroundColor: Theme.of(context).cardColor,
            title: GestureDetector(
              onTap: () => Routes.sailor
                  .navigate('/', navigationType: NavigationType.pushReplace),
              child: Image(
                  height: 70, image: AssetImage('assets/emetal-logo.png')),
            ),
            actions: constraints.minWidth > 800
                ? [
                    "어떻게 작동하죠 ?".navBtn(onPressedCb: () {
                      pageController.animateToPage(1,
                          duration: Duration(milliseconds: 400),
                          curve: Curves.decelerate);
                    }),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: TextButton(
                        style: TextButton.styleFrom(
                          padding: EdgeInsets.symmetric(vertical: 0.0),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                        ),
                        child: "우리에 대해".styled(btnLabelStyle),
                        onPressed: () async {
                          if (await canLaunch(
                              'https://www.trillionslab.com/')) {
                            await launch(
                              'https://www.trillionslab.com/',
                              forceSafariVC: true,
                              forceWebView: true,
                              enableJavaScript: true,
                            );
                          } else {
                            throw 'Could not launch https://www.trillionslab.com/';
                          }
                        },
                      ),
                    ),
                    "연락처 지원".navBtn(onPressedCb: () async {
                      return await Navigator.of(context)
                          .push(new PageRouteBuilder(
                              opaque: false,
                              pageBuilder: (BuildContext context, _, __) {
                                return ContactUs();
                              }));
                    }),
                    user != null
                        ? Padding(
                            padding: EdgeInsets.symmetric(horizontal: 20),
                            child: InkWell(
                              onTap: () {
                                Routes.sailor.navigate('/profile');
                              },
                              child: CachedNetworkImage(
                                imageUrl: user['avatar_url'] != null
                                    ? user['avatar_url']
                                    : 'https://i.ibb.co/0KDwYTL/user-icon-male-hipster-avatar-vector-flat-design-user-icon-male-beard-icon-hipster-flat-icon-avatar.jpg',
                                imageBuilder: (context, imageProvider) =>
                                    Container(
                                  height: 50,
                                  width: 50,
                                  decoration: BoxDecoration(
                                      image: DecorationImage(
                                        fit: BoxFit.cover,
                                        image: imageProvider,
                                      ),
                                      shape: BoxShape.circle),
                                ),
                                placeholder: (context, url) => Padding(
                                  padding: EdgeInsets.symmetric(vertical: 20),
                                  child: CircularProgressIndicator.adaptive(),
                                ),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                            ),
                          )
                        : "로그인".navBtn(onPressedRoute: "/sign-in"),
                  ]
                : [
                    IconButton(
                        icon: Icon(
                          Icons.person,
                          color: Colors.grey.shade600,
                        ),
                        onPressed: () => Routes.sailor("/profile")),
                  ],
          ),
        ),
      );
  const CustomAppBar({Key key, @required this.pageController, this.user})
      : super(key: key);

  final PageController pageController;

  final dynamic user;
  @override
  Widget build(BuildContext context) {
    return child;
  }
}

class NavbarHook {
  final appBar, drawer, pageController, user;
  const NavbarHook({this.drawer, this.appBar, this.pageController, this.user});
}
