import 'package:flutter/material.dart';
import 'package:layout/layout.dart';

StyleHook useStyles(context) {
  return StyleHook(context: context);
}

class StyleHook {
  final BuildContext context;
  get h1Size => context.layout.value(xs: 16.0, sm: 18, md: 20);
  get h2Size => context.layout.value(xs: 14.0, sm: 16, md: 18);
  get t1Size => context.layout.value(xs: 12.0, sm: 14, md: 16);
  get width => MediaQuery.of(context).size.width;
  get height => MediaQuery.of(context).size.height;
  get headerBorderRadius =>
      context.layout.value(xs: 20, sm: 25, md: 40, lg: 45);
  get dashBoardHeaderHeight => context.layout.value(
        xs: 200,
        sm: 250,
        md: 300,
      );
  get searchBarWidth => context.layout.value(xs: 300);
  get searchBarCenterOffset =>
      width * 0.5 - searchBarWidth / context.layout.value(xs: 1.55, md: 1.5);
  StyleHook({this.context});
}
