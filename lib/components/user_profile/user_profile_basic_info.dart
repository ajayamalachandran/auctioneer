import 'package:auctioneer/style.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';

class UserProfileBasicInfo extends HookWidget {
  final userInfo;
  const UserProfileBasicInfo({
    @required this.userInfo,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      decoration: BoxDecoration(
        color: Theme.of(context).cardColor,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [
          BoxShadow(
            color: Colors.black.withOpacity(0.2),
            blurRadius: 6.0,
            spreadRadius: 2.0,
            offset: Offset(
              5.0,
              5.0,
            ),
          )
        ],
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: [
          CachedNetworkImage(
            imageUrl: userInfo['avatar_url'] != null
                ? userInfo['avatar_url']
                : 'https://180dc.org/wp-content/uploads/2017/11/profile-placeholder.png',
            imageBuilder: (context, imageProvider) => Container(
              width: 90,
              height: 90,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
              ),
            ),
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
          SizedBox(width: 20),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              '${userInfo['name']}'.styled(profileUserNameText(context)),
              SizedBox(
                height: 10,
              ),
              '${userInfo['email']}'.styled(userProfileAddress(context)),
              SizedBox(height: 10),
              '${userInfo['phone_number']}'.styled(userProfileAddress(context)),
            ],
          ),
        ],
      ),
    );
  }
}
