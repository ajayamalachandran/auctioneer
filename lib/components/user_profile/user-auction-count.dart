import 'package:auctioneer/style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';
import 'package:countup/countup.dart';

class UserAuctionCount extends HookWidget {
  final userInfo;
  const UserAuctionCount({
    @required this.userInfo,
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            'My Auctions'.styled(profileUserNameText(context)),
            SizedBox(
              height: 10,
            ),
            Countup(
              begin: 0,
              end: 200,
              duration: Duration(seconds: 3),
              separator: ',',
              style: TextStyle(
                fontSize: 24,
                color: Theme.of(context).primaryColorDark,
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
        Column(
          children: [
            'My Biddings'.styled(profileUserNameText(context)),
            SizedBox(
              height: 10,
            ),
            Countup(
              begin: 0,
              end: 1320,
              duration: Duration(seconds: 3),
              separator: ',',
              style: TextStyle(
                fontSize: 24,
                color: Theme.of(context).primaryColorDark,
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
        Column(
          children: [
            'My Bidders'.styled(profileUserNameText(context)),
            SizedBox(
              height: 10,
            ),
            Countup(
              begin: 0,
              end: 7500,
              duration: Duration(seconds: 3),
              separator: ',',
              style: TextStyle(
                fontSize: 24,
                color: Theme.of(context).primaryColorDark,
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
      ],
    );
  }
}
