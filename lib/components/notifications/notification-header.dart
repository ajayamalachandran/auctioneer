import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/components/notifications/notification-user-image.dart';
import 'package:auctioneer/custom.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/extensions.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

class NotificationHeader extends HookWidget {
  final String kind, imageUrl, name, price, unitsAvailable, date, message;
  const NotificationHeader(
      {Key key,
      this.unitsAvailable = "180",
      this.name = "Hard Fibre Metal Pipes",
      this.price = "2,35,000",
      this.date = "24th March 2021 at 12.30 PM",
      this.kind = "Buying",
      this.message =
          "charlie made a new bid for your auction jackhammer for £100.",
      this.imageUrl = "https://i.ibb.co/V9bBvnB/rods.jpg"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formatter = NumberFormat('##,##,##,##,000');
    double width = MediaQuery.of(context).size.width;

    return Container(
      width: width,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          border:
              Border(bottom: BorderSide(color: Colors.grey.withOpacity(0.2)))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          NotificationUserImage(imageUrl: imageUrl, kind: kind),
          SizedBox(width: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              kindTile(
                kind == 'bid_received'
                    ? '새로운 입찰'
                    : kind == 'bid_won'
                        ? '낙찰'
                        : kind == 'bid_lost'
                            ? '입찰 실패'
                            : kind == 'auction_sold'
                                ? '매진'
                                : '팔리지 않은',
                color: Theme.of(context).primaryColorDark,
              ),
              SizedBox(height: 5),
              Container(
                  width: width * .7,
                  child:
                      "$message".styled(notificationShortStartText(context))),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  iconLabelNotification(
                      icon: Icons.timer,
                      color: Colors.grey.withOpacity(0.77),
                      label: timeago.format(DateTime.parse(date))),
                  SizedBox(
                    width: 20,
                  ),
                  // "원 ${formatter.format(double.parse(price))} / 톤"
                  //     .styled(auctionPriceNotificationStyle),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
