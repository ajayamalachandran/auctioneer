import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class NotificationUserImage extends HookWidget {
  final String kind, imageUrl;
  final double height,
      width,
      right,
      bottom,
      overlayHeight,
      overlayWidth,
      iconSize;
  const NotificationUserImage({
    Key key,
    this.kind = 'new_bid',
    this.imageUrl = "https://i.ibb.co/WtmBb70/metal-tubes.jpg",
    this.height = 40,
    this.width = 40,
    this.right = -8,
    this.bottom = -15,
    this.overlayHeight = 15,
    this.overlayWidth = 15,
    this.iconSize = 8,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl != null
          ? imageUrl
          : 'https://i.ibb.co/WtmBb70/metal-tubes.jpg',
      imageBuilder: (context, imageProvider) => Container(
          height: height,
          width: width,
          padding: EdgeInsets.symmetric(vertical: 20),
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(image: imageProvider, fit: BoxFit.cover),
          ),
          child: kind != "bidder"
              ? Stack(
                  overflow: Overflow.visible,
                  children: [
                    Positioned(
                      right: right,
                      bottom: bottom,
                      child: Container(
                        height: overlayHeight,
                        width: overlayWidth,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Theme.of(context).accentColor,
                        ),
                        child: Icon(
                          kind == 'item_sold'
                              ? Icons.gavel_outlined
                              : kind == 'bid_won'
                                  ? Icons.thumb_up
                                  : kind == 'bid_lost'
                                      ? Icons.thumb_down
                                      : kind == 'item_unsold'
                                          ? Icons.close
                                          : Icons.chat_bubble,
                          color: Colors.white,
                          size: iconSize,
                        ),
                      ),
                    ),
                  ],
                )
              : Container()),
      placeholder: (context, url) => CircularProgressIndicator(),
      errorWidget: (context, url, error) => Icon(Icons.error),
    );
  }
}
