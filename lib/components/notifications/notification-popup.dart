import 'package:auctioneer/components/notifications/notification-user-image.dart';
import 'package:auctioneer/custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:layout/layout.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/extensions.dart';

class NotificationPopUp extends HookWidget {
  final String kind, imageUrl, name, price, unitsAvailable, date;
  const NotificationPopUp(
      {Key key,
      this.unitsAvailable = "180",
      this.name = "Hard Fibre Metal Pipes",
      this.price = "2,35,000",
      this.date = "24th March 2021 at 12.30 PM",
      this.kind = "Buying",
      this.imageUrl = "https://i.ibb.co/WtmBb70/metal-tubes.jpg"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Container(
        width: width,
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: BoxDecoration(
            color: Theme.of(context).cardColor,
            border:
                Border(bottom: BorderSide(color: Colors.grey[200], width: 2))),
        child: Wrap(
          verticalDirection: VerticalDirection.down,
          runSpacing: 20,
          spacing: 20,
          direction: context.breakpoint > LayoutBreakpoint.sm
              ? Axis.horizontal
              : Axis.vertical,
          alignment: WrapAlignment.spaceBetween,
          runAlignment: context.breakpoint > LayoutBreakpoint.sm
              ? WrapAlignment.start
              : WrapAlignment.center,
          crossAxisAlignment: context.breakpoint > LayoutBreakpoint.sm
              ? WrapCrossAlignment.center
              : WrapCrossAlignment.center,
          children: [
            Wrap(
              direction: Axis.horizontal,
              alignment: WrapAlignment.start,
              runAlignment: WrapAlignment.start,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                NotificationUserImage(
                  imageUrl: imageUrl,
                  kind: kind,
                  height: 50,
                  width: 50,
                  bottom: -20,
                  overlayHeight: 20,
                  overlayWidth: 20,
                  iconSize: 10,
                ),
                SizedBox(width: 20),
                Wrap(
                  direction: Axis.vertical,
                  alignment: WrapAlignment.start,
                  runAlignment: WrapAlignment.start,
                  crossAxisAlignment: context.breakpoint > LayoutBreakpoint.sm
                      ? WrapCrossAlignment.start
                      : WrapCrossAlignment.start,
                  spacing: 10,
                  children: [
                    Container(
                        width: width > 500 ? width * .4 : width * .8,
                        child:
                            "charlie made a new bid for your auction jackhammer for £100."
                                .styled(notificationShortStartPopUpText)),
                    Wrap(
                      direction: Axis.horizontal,
                      alignment: WrapAlignment.center,
                      runAlignment: WrapAlignment.center,
                      spacing: 10,
                      children: [
                        iconLabelNotification(
                            icon: Icons.timer,
                            color: Colors.grey.withOpacity(0.77),
                            label: '2 days ago'),
                        SizedBox(width: 10),
                        "원 $price / ton"
                            .styled(auctionPriceNotificationPopUpStyle),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ],
        ));
  }
}
