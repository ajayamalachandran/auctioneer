import 'package:auctioneer/components/notifications/notification-header.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class NotificationCard extends HookWidget {
  final String kind, imageUrl, name, price, unitsAvailable, date, message;
  final notificationInfo;
  const NotificationCard(
      {Key key,
      this.unitsAvailable = "180",
      this.name = "Hard Fibre Metal Pipes",
      this.price = "2,35,000",
      this.date = "24th March 2021 at 12.30 PM",
      this.kind = "Buying",
      this.message =
          "charlie made a new bid for your auction jackhammer for £100.",
      this.imageUrl = "https://i.ibb.co/WtmBb70/metal-tubes.jpg",
      this.notificationInfo})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NotificationHeader(
      date: date,
      imageUrl: imageUrl,
      kind: kind,
      name: name,
      price: price,
      message: message,
    );
  }
}
