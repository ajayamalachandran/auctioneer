import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/custom.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/extensions.dart';
import 'package:intl/intl.dart';
import 'package:url_launcher/url_launcher.dart';

class NotificationExpanded extends HookWidget {
  final String kind, imageUrl, name, price, unitsAvailable, date;
  final notificationInfo;
  const NotificationExpanded(
      {Key key,
      this.unitsAvailable = "180",
      this.name = "Hard Fibre Metal Pipes",
      this.price = "2,35,000",
      this.date = "24th March 2021 at 12.30 PM",
      this.kind = "Buying",
      this.notificationInfo,
      this.imageUrl = "https://i.ibb.co/WtmBb70/metal-tubes.jpg"})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    final formatter = NumberFormat('###,###,###,###,000');
    int endTime = DateTime.parse(notificationInfo['auction']['end_time'])
            .millisecondsSinceEpoch +
        1000 * 30;
    return Container(
      decoration: BoxDecoration(
        color: Colors.grey[200].withOpacity(0.7),
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(10), topRight: Radius.circular(10)),
      ),
      width: width,
      alignment: Alignment.center,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          '${notificationInfo['auction']['description'] != null ? notificationInfo['auction']['description'] : ''}'
              .styled(auctionTimeTypeText),
          Container(
            alignment: Alignment.center,
            child: Wrap(
              direction: Axis.vertical,
              alignment: WrapAlignment.center,
              runAlignment: WrapAlignment.spaceBetween,
              spacing: 10,
              runSpacing: 10,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Wrap(direction: Axis.horizontal, children: [
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        "원 ${notificationInfo['auction']['my_bid'] != null ? formatter.format(double.parse(notificationInfo['auction']['my_bid']['bid_amount'])) : '000'} / ton"
                            .styled(auctionTimeText),
                        "현재 입찰가".styled(auctionTimeTypeText),
                      ]),
                  SizedBox(height: 20, width: 20),
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        "원 ${formatter.format(double.parse(notificationInfo['auction']['best_bid']['bid_amount']))} / ton"
                            .styled(auctionTimeText),
                        "최고 입찰가".styled(auctionTimeTypeText),
                      ]),
                ]),
                CountdownTimer(
                  endTime: endTime,
                  widgetBuilder: (_, CurrentRemainingTime time) {
                    if (time == null) {
                      return Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          expandTimerColumn(children: [
                            '종료'.styled(auctionTimeText),
                            "상태".styled(auctionTimeTypeText)
                          ]),
                        ],
                      );
                    } else {
                      return Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          expandTimerColumn(children: [
                            '${time.days != null ? time.days : '0'}'
                                .styled(auctionTimeText),
                            "Days".styled(auctionTimeTypeText)
                          ]),
                          expandTimerColumn(children: [
                            '${time.hours != null ? time.hours : '0'}'
                                .styled(auctionTimeText),
                            "Hours".styled(auctionTimeTypeText)
                          ]),
                          expandTimerColumn(children: [
                            '${time.min != null ? time.min : '0'}'
                                .styled(auctionTimeText),
                            "Minutes".styled(auctionTimeTypeText)
                          ]),
                          expandTimerColumn(children: [
                            '${time.sec != null ? time.sec : '0'}'
                                .styled(auctionTimeText),
                            "Seconds".styled(auctionTimeTypeText)
                          ]),
                        ],
                      );
                    }
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    notificationInfo['auction']['winning_tx_link'] != null
                        ? Tooltip(
                            message: '사용자 프로필로 이동',
                            child: expandedButton(
                                onTap: () async {
                                  if (await canLaunch(
                                      notificationInfo['auction']
                                          ['winning_tx_link'])) {
                                    await launch(
                                      notificationInfo['auction']
                                          ['winning_tx_link'],
                                      forceSafariVC: true,
                                      forceWebView: true,
                                      enableJavaScript: true,
                                    );
                                  } else {
                                    throw 'Could not launch ${notificationInfo['auction']['winning_tx_link']}';
                                  }
                                },
                                icon: Icon(
                                  Icons.security,
                                  color: Colors.white,
                                  size: 21,
                                )),
                          )
                        : SizedBox(
                            height: 10,
                            width: 10,
                          ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
