import 'package:auctioneer/components/bid-details/bidder-tile.dart';
import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/core/loader.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/extensions.dart';

class BiddersList extends HookWidget {
  final auction;
  const BiddersList({Key key, this.auction}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final bids = useValueNotifier([]);

    useEffect(() {
      getAuctionBids(auction['id']).pipe(bids);
    }, []);

    useListenable(bids);
    return bids.value.length > 0
        ? Scaffold(
            backgroundColor: Theme.of(context).backgroundColor,
            body: SingleChildScrollView(
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.symmetric(vertical: 20),
                child: Wrap(
                  spacing: 10,
                  direction: Axis.horizontal,
                  children: bids.value.map<Widget>((bid) {
                    return BidderTile(
                      bid: bid,
                    );
                  }).toList(),
                ),
              ),
            ))
        : Loader();
  }
}
