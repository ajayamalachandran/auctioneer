import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:auctioneer/components/notifications/notification-user-image.dart';
import 'package:auctioneer/custom.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/extensions.dart';
import 'package:intl/intl.dart';
import 'package:timeago/timeago.dart' as timeago;

class BidderTile extends HookWidget {
  final bid;
  const BidderTile({Key key, this.bid}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final formatter = NumberFormat('##,##,##,##,000');
    double width = MediaQuery.of(context).size.width;
    return Container(
      width: width,
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
          border:
              Border(bottom: BorderSide(color: Colors.grey.withOpacity(0.2)))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          NotificationUserImage(
              imageUrl: bid['user']['avatar_url'] != null
                  ? bid['user']['avatar_url']
                  : 'https://i.ibb.co/WtmBb70/metal-tubes.jpg',
              kind: 'bidder'),
          SizedBox(width: 20),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  width: width * .6,
                  child: "${bid['user']['name']}"
                      .styled(notificationShortStartText(context))),
              SizedBox(height: 5),
              timeago
                  .format(DateTime.parse(bid['created_at']))
                  .styled(notificationTime)
            ],
          ),
          ' \$ ${formatter.format(double.parse(bid['bid_amount']))}'
              .styled(auctionPriceNotificationStyle(context))
        ],
      ),
    );
  }
}
