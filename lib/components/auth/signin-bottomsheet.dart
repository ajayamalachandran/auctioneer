import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class SignInBackground extends HookWidget {
  final Widget child;
  const SignInBackground({Key key, this.child}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.transparent,
      bottomSheet: child,
      body: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Stack(
          children: [
            Container(
              child: Image.asset(
                'assets/auction.jpg',
                height: size.height,
                width: size.width,
                fit: BoxFit.cover,
              ),
            )
          ],
        ),
      ),
    );
  }
}
