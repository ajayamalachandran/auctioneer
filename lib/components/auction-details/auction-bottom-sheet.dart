import 'package:auctioneer/core/api.dart';
import 'package:auctioneer/core/flutter-toast.dart';
import 'package:auctioneer/hooks/use-styles.dart';
import 'package:auctioneer/style.dart';
import 'package:auctioneer/widgets/text_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';
import 'package:auctioneer/extensions.dart';

class AuctionDetailsBottomsheet extends HookWidget {
  final auction;
  const AuctionDetailsBottomsheet({Key key, this.auction}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bidPrice = useValueNotifier(MoneyMaskedTextController(
        decimalSeparator: '', thousandSeparator: ',', precision: 0));
    final totalPriceNotifier = useValueNotifier('0');
    final formatterNonDecimal = NumberFormat('###,###,###,###,000');

    final styleHook = useStyles(context);

    String getTotalLabelValue() {
      return (double.parse(auction['units_available']) *
              double.parse(bidPrice.value.numberValue > 0
                  ? bidPrice.value.numberValue.toString()
                  : '0'))
          .toString();
    }

    useListenable(bidPrice);
    return Container(
      decoration: BoxDecoration(color: Theme.of(context).cardColor),
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      child: Wrap(
        spacing: 20,
        direction: Axis.horizontal,
        alignment: WrapAlignment.spaceEvenly,
        crossAxisAlignment: WrapCrossAlignment.center,
        runAlignment: WrapAlignment.spaceEvenly,
        children: [
          Wrap(
            spacing: 10,
            direction: Axis.vertical,
            alignment: WrapAlignment.center,
            crossAxisAlignment: WrapCrossAlignment.center,
            runAlignment: WrapAlignment.center,
            children: [
              '입찰 가격 / 단위'
                  .styled(yourBidPriceStyle..fontSize(styleHook.h2Size)),
              SizedBox(height: 10),
              Form(
                key: Key(auction['name']),
                child: TextInputWithPrefixSuffixMaterial(
                  inputType: TextInputType.number,
                  width: 200,
                  backgroundColor: Colors.grey[100],
                  borderRadius: 10,
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  hint: '',
                  controller: bidPrice.value,
                  onChange: (String value) {
                    totalPriceNotifier.value = getTotalLabelValue();
                  },
                ),
              ),
              '총량 :  ${formatterNonDecimal.format(double.parse(totalPriceNotifier.value))} 톤'
                  .styled(auctionPriceStyle..fontSize(styleHook.h2Size)),
            ],
          ),
          Wrap(
            children: [
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        side: BorderSide(width: 1, color: Colors.redAccent),
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.all(20),
                      elevation: 4,
                      primary: Colors.grey.shade50,
                      onPrimary: Colors.red.withOpacity(0.2)),
                  onPressed: () async {
                    if ((auction['kind'] == 'normal_auction' &&
                            bidPrice.value.numberValue >=
                                double.parse(auction['unit_price'])) ||
                        (auction['kind'] == 'reverse_auction' &&
                            bidPrice.value.numberValue <=
                                double.parse(auction['unit_price']))) {
                      final bidResponse = await placeBid({
                        'bid': {
                          'auction_id': auction['id'],
                          'bid_amount': totalPriceNotifier.value
                        }
                      });
                      if (bidResponse['error'] == null) {
                        bidPrice.value.text = '0.00';
                        FlutterToast.showSuccess(
                            context: context, message: bidResponse['message']);
                      } else {
                        FlutterToast.showErrorToast(
                            context: context, message: bidResponse['message']);
                      }
                    } else {
                      FlutterToast.showErrorToast(
                          context: context,
                          message: 'Make the appropriate bid');
                    }
                  },
                  child: "경매 참가 (응찰)"
                      .styled(placeBidTextStyle..fontSize(styleHook.h2Size)))
            ],
          )
        ],
      ),
    );
  }
}
