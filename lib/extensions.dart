import 'dart:convert';

import 'package:auctioneer/routes.dart';
import 'package:division/division.dart';
import 'package:auctioneer/style.dart';
import 'package:flutter/material.dart';
// import 'package:flutter_screenutil/flutter_screenutil.dart';

extension TextDivisionExtension on String {
  styled(style) {
    return Txt(this, style: style);
  }
}

extension JsonExtension on List {
  get json {
    final jsonEncoder = JsonEncoder();
    return jsonEncoder.convert(this).replaceAllMapped(new RegExp(r'"(\w+)":'),
        (match) {
      return "${match[1]}:";
    });
  }
}

extension WidgetDivisionExtension on Widget {
  styled(style) {
    return Parent(child: this, style: style);
  }
}

extension TextButtonExtension on String {
  navBtn({String onPressedRoute, Function onPressedCb}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8.0),
      child: TextButton(
        style: TextButton.styleFrom(
          padding: EdgeInsets.symmetric(vertical: 0.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
        child: this.styled(btnLabelStyle),
        onPressed: () {
          if (onPressedCb == null) {
            return Routes.sailor.navigate(onPressedRoute);
          }
          onPressedCb();
        },
      ),
    );
  }
}

extension ExpandExtension on Widget {
  expandFlex(flex) => Expanded(child: this, flex: flex);
}

// extension SpacingExtension on num {
//   get vSizedBox => SizedBox(height: ScreenUtil().setHeight(this));
//   get hSizedBox => SizedBox(width: ScreenUtil().setWidth(this));
// }

extension ApiExtension on Future<dynamic> {
  pipe(valueNotifier) {
    this.then((receivedValue) {
      valueNotifier.value = receivedValue;
    });
  }

  // pipePage(valueNotifier, pageInfoNotifier) {
  //   this.then((response) {
  //     valueNotifier.value = valueNotifier.value + response["nodes"];
  //     pageInfoNotifier.value = response["pageInfo"];
  //   });
  // }
}
