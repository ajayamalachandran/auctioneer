import 'package:division/division.dart';
import 'package:flutter/material.dart';

final navbarLogoStyle = TxtStyle()
  ..textAlign.left()
  ..bold()
  ..fontSize(24)
  ..textColor(Colors.white)
  ..letterSpacing(1.2);

final btnLabelStyle = TxtStyle()
  ..textColor(Colors.grey.shade800)
  ..fontSize(16)
  ..padding(vertical: 5, horizontal: 10)
  ..letterSpacing(1.1);

final introTitleStyle = TxtStyle()
  ..textAlign.left()
  ..bold()
  ..fontSize(55)
  ..textColor(Colors.grey.shade800)
  ..letterSpacing(2)
  ..textShadow(
      color: Colors.grey.shade400.withOpacity(0.5), offset: Offset(4, 4));

final introTitleStyleSmall = TxtStyle()
  ..add(introTitleStyle)
  ..fontSize(35)
  ..textAlign.center();

final authBtnText = TxtStyle()
  ..textColor(Colors.white)
  ..fontSize(14)
  ..fontWeight(FontWeight.bold)
  ..textAlign.center();

final howItWorksTitleStyle = TxtStyle()
  ..textAlign.center()
  ..bold()
  ..fontSize(55)
  ..padding(vertical: 50)
  ..border(top: 4, bottom: 8, color: Colors.deepOrangeAccent)
  ..textColor(Colors.grey.shade800)
  ..letterSpacing(6);

final inputHint = TxtStyle()
  ..textColor(Colors.grey[700])
  ..fontSize(12);

final welcomeTitle = TxtStyle()
  ..textColor(Colors.black)
  ..fontWeight(FontWeight.w600)
  ..fontSize(24);

final profileSideHeading = (context) => TxtStyle()
  ..textColor(
    Theme.of(context).accentColor,
  )
  ..fontWeight(FontWeight.bold)
  ..fontSize(19);

final forgetPasswordText = TxtStyle()
  ..textColor(Colors.black)
  ..fontWeight(FontWeight.bold)
  ..fontSize(14);

final profileUserNameText = (context) => TxtStyle()
  ..textColor(
    Theme.of(context).primaryColorDark,
  )
  ..fontWeight(FontWeight.bold)
  ..fontSize(16);

final profileUserCountUpText = (context) => TxtStyle()
  ..textColor(
    Theme.of(context).primaryColorDark,
  )
  ..fontWeight(FontWeight.bold)
  ..fontSize(20);

final userProfileAddress = (context) => TxtStyle()
  ..textColor(
    Theme.of(context).primaryColorDark,
  )
  ..fontSize(14);

final profileLocationText = TxtStyle()
  ..textColor(Colors.grey.withOpacity(0.57))
  ..fontSize(14);

final notificationBackgroundText = TxtStyle()
  ..textColor(Colors.grey[100].withOpacity(0.6))
  ..fontWeight(FontWeight.w900)
  ..fontSize(100);
final auctionTitleStyle = (context) => TxtStyle()
  ..textColor(
    Theme.of(context).primaryColorDark,
  )
  ..fontSize(18)
  ..letterSpacing(1.5)
  ..bold();

final auctionTileTitleText = (context) => TxtStyle()
  ..textColor(
    Theme.of(context).primaryColorDark,
  )
  ..fontSize(14)
  ..bold();

final auctionDescriptionStyle = TxtStyle()
  ..textColor(Colors.grey[400])
  ..fontSize(14);

final auctionCountDownStyle = (context) => TxtStyle()
  ..textColor(
    Theme.of(context).primaryColorDark,
  )
  ..fontWeight(FontWeight.bold)
  ..fontSize(14);
final auctionCountDownTileStyle = (context) => TxtStyle()
  ..textColor(
    Theme.of(context).primaryColorDark,
  )
  ..fontSize(12);
final commonAppbarText = (context) => TxtStyle()
  ..textColor(
    Theme.of(context).primaryColorDark,
  )
  ..fontWeight(FontWeight.bold)
  ..fontSize(18);
final auctionCardPrice = TxtStyle()
  ..textColor(Colors.white)
  ..fontWeight(FontWeight.bold)
  ..fontSize(14);

final auctionSubStyle = TxtStyle()
  ..textColor(Colors.grey.withOpacity(0.77))
  ..fontSize(18)
  ..letterSpacing(1.5);

final notificationTime = TxtStyle()
  ..textColor(Colors.grey.withOpacity(0.77))
  ..fontSize(12);

final auctionPriceStyle = TxtStyle()
  ..textColor(Colors.blueGrey.withOpacity(0.77))
  ..fontSize(20)
  ..bold()
  ..letterSpacing(1.5);

final auctionPriceNotificationStyle = (context) => TxtStyle()
  ..textColor(
    Theme.of(context).primaryColorDark,
  )
  ..fontSize(12)
  ..bold();

final auctionPriceNotificationPopUpStyle = TxtStyle()
  ..textColor(Colors.orange[700])
  ..fontSize(10)
  ..bold();

final yourBidPriceStyle = TxtStyle()
  ..textColor(Colors.redAccent.withOpacity(0.77))
  ..fontSize(20)
  ..bold()
  ..letterSpacing(1.5);

final placeBidTextStyle = TxtStyle()
  ..textColor(Colors.redAccent)
  ..fontSize(18)
  ..bold()
  ..letterSpacing(1.5);

final kindText = TxtStyle()
  ..textColor(Colors.white)
  ..fontWeight(FontWeight.bold)
  ..fontSize(10);

final kindTextPopup = TxtStyle()
  ..textColor(Colors.white)
  ..fontWeight(FontWeight.bold)
  ..fontSize(10);

final notificationNew = TxtStyle()
  ..textColor(Colors.white)
  ..fontWeight(FontWeight.bold)
  ..fontSize(14);

final notificationShortCenteredText = TxtStyle()
  ..textColor(Colors.black.withOpacity(0.77))
  ..textOverflow(TextOverflow.clip)
  ..textAlign.center()
  ..fontSize(14)
  ..fontWeight(FontWeight.bold);

final notificationShortStartText = (context) => TxtStyle()
  ..textColor(
    Theme.of(context).primaryColorDark,
  )
  ..textOverflow(TextOverflow.clip)
  ..textAlign.start()
  ..fontSize(14)
  ..fontWeight(FontWeight.bold);

final notificationAppBarTitle = (context) => TxtStyle()
  ..textColor(
    Theme.of(context).primaryColorDark,
  )
  ..textAlign.start()
  ..fontSize(18)
  ..fontWeight(FontWeight.bold);

final auctionEndingInText = TxtStyle()
  ..textColor(Colors.black)
  ..textOverflow(TextOverflow.clip)
  ..fontSize(16)
  ..fontWeight(FontWeight.bold);

final auctionTimeText = TxtStyle()
  ..padding(vertical: 10)
  ..textColor(Colors.black)
  ..textOverflow(TextOverflow.clip)
  ..fontSize(24)
  ..fontWeight(FontWeight.w600);

final auctionTimeTypeText = TxtStyle()
  ..textColor(Color(0xff666666))
  ..textOverflow(TextOverflow.clip)
  ..textAlign.start()
  ..fontSize(16)
  ..fontWeight(FontWeight.w600);

final notificationShortStartPopUpText = TxtStyle()
  ..textColor(Colors.black.withOpacity(0.77))
  ..textOverflow(TextOverflow.clip)
  ..textAlign.start()
  ..fontSize(12)
  ..fontWeight(FontWeight.bold);

final notificationNameText = TxtStyle()
  ..textColor(Colors.red.withOpacity(0.77))
  ..fontSize(14)
  ..fontWeight(FontWeight.bold);

final notificationLongText = TxtStyle()
  ..textColor(Colors.black.withOpacity(0.77))
  ..fontSize(12);

final noDataFoundText = TxtStyle()
  ..textColor(Colors.orange[700])
  ..fontSize(12);

final signinOptionsText = TxtStyle()
  ..fontSize(16)
  ..textAlign.center();
