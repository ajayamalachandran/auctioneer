import 'package:dio/dio.dart';
import 'package:auctioneer/core/local-storage.dart';

class Client {
  static final String url = 'http://192.168.1.6:3000/';
  // static final String url = 'https://stage-admin.emetalexchange.com/';

  static BaseOptions opts = BaseOptions(
    baseUrl: url,
    responseType: ResponseType.json,
    // connectTimeout: 30000,
    // receiveTimeout: 30000,
  );

  static Dio createDio() {
    return Dio(opts);
  }

  static Dio addInterceptors(Dio dio) {
    return dio
      ..interceptors.add(
        InterceptorsWrapper(
          onRequest: (options, handler) => requestInterceptor(options, handler),
          onError: (e, handler) {
            return handler.next(e.response.data);
          },
          // onRequest: (RequestOptions options) => requestInterceptor(options),
          // onError: (DioError e) async {
          //   return e.response.data;
          // },
        ),
      );
  }

  static dynamic requestInterceptor(
      RequestOptions options, RequestInterceptorHandler handler) async {
    // Get your JWT token
    final token = await SecureStorage.getToken();
    options.headers.addAll({"authorization": "$token"});
    return handler.next(options);
  }

  static final dio = createDio();
  static final baseAPI = addInterceptors(dio);

  static Future<Response> getHTTP(String url, {queryparams, options}) async {
    try {
      Response response = await baseAPI.get(url,
          queryParameters: queryparams, options: options);
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }

  static Future<Response> postHTTP(String url, dynamic data) async {
    try {
      Response response = await baseAPI.post(url, data: data);
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }

  static Future<Response> putHTTP(String url, dynamic data) async {
    try {
      Response response = await baseAPI.put(url, data: data);
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }

  static Future<Response> deleteHTTP(String url) async {
    try {
      Response response = await baseAPI.delete(url);
      return response;
    } on DioError catch (e) {
      return e.response;
    }
  }
}
