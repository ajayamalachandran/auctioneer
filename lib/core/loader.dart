import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loader extends StatefulWidget {
  final String header;
  final double height;
  final Color backgroundColor;

  Loader({this.header, this.height, this.backgroundColor});

  @override
  _LoaderState createState() => _LoaderState();
}

class _LoaderState extends State<Loader> {
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: widget.backgroundColor != null
          ? widget.backgroundColor
          : Theme.of(context).primaryColorDark,
      body: Container(
        height: widget.height != null ? widget.height : size.height,
        width: size.width,
        child: Center(
          child: SpinKitDoubleBounce(
            color: Colors.white,
            size: 100,
          ),
        ),
      ),
    );
  }
}
