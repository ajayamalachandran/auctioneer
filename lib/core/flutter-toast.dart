import 'package:toast/toast.dart';
import 'package:flutter/material.dart';

class FlutterToast {
  static void show({String message, BuildContext context}) {
    Toast.show(
      message,
      context,
      duration: Toast.LENGTH_LONG,
      gravity: Toast.BOTTOM,
    );
  }

  static void showErrorToast({
    String message,
    BuildContext context,
    int gravity = 0,
  }) {
    SnackBar(
      content: Text(message),
      backgroundColor: Colors.red,
      behavior: SnackBarBehavior.fixed,

      // message,
      // context,
      // duration: Toast.LENGTH_LONG,
      // gravity: gravity,
      // backgroundColor: Colors.red,
      // textColor: Colors.white,
      // backgroundRadius: 2,
    );
  }

  static void showSuccess({String message, BuildContext context}) {
    SnackBar(
      content: Text(message), backgroundColor: Color(0xff18a25d),

      // message,
      // context,
      // duration: Toast.LENGTH_LONG,
      // gravity: Toast.BOTTOM,
      // backgroundColor: Color(0xff18a25d),
      // textColor: Colors.white,
      // backgroundRadius: 2,
    );
  }
}
