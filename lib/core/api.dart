import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'package:auctioneer/core/client.dart';
import 'package:auctioneer/core/local-storage.dart';

Future<dynamic> login(postData) async {
  final loginResponse = await Client.putHTTP('login.json', postData);
  return loginResponse.data;
}

Future<bool> isLoggedIn() async {
  final token = await SecureStorage.getToken();
  return token != null;
}

Future<String> getTheme() async {
  final theme = await SecureStorage.getSecureDataLocally(key: 'theme');
  return theme;
}

Future<dynamic> signUp(postData) async {
  final createUser = await Client.postHTTP('/users.json', postData);
  return createUser.data;
}

Future<dynamic> getMe() async {
  final meResponse = await Client.getHTTP('/me.json');
  return meResponse.data;
}

Future<dynamic> updateProfile(postData) async {
  final meResponse = await Client.putHTTP('/me', postData);
  return meResponse.data;
}

Future<dynamic> createAuction(postData) async {
  final meResponse = await Client.postHTTP('/auctions.json', postData);
  return meResponse.data;
}

Future<dynamic> getMyNotifications() async {
  final notificationsRespose = await Client.getHTTP('/notifications.json');
  return notificationsRespose.data;
}

Future<dynamic> getVarients() async {
  final respose = await Client.getHTTP('/variants.json');
  return respose.data;
}

Future<dynamic> getCategories() async {
  final respose = await Client.getHTTP('/categories.json');
  return respose.data;
}

Future<dynamic> getAuctions(type, query, searchQuery) async {
  final respose = await Client.getHTTP(type != null
      ? '/auctions.json?$type=$query&query=$searchQuery'
      : '/auctions.json?query=$searchQuery');
  return respose.data;
}

Future<dynamic> getAuction(id) async {
  final respose = await Client.getHTTP('/auction.json?id=$id');
  return respose.data;
}

Future<dynamic> getAuctionBids(id) async {
  final respose = await Client.getHTTP('/bids.json?auction_id=$id');
  return respose.data;
}

Future<dynamic> placeBid(postData) async {
  final respose = await Client.postHTTP('/place-bid.json', postData);
  return respose.data;
}

Future<dynamic> getSignedUrl(filename) async {
  final respose = await Client.postHTTP('/signed-url?key=$filename', {});
  return respose.data;
}

Future<String> uploadImage(String signedUrl, File image) async {
  var response = await http.put(Uri.parse(signedUrl),
      body: image.readAsBytesSync(),
      headers: {Headers.contentTypeHeader: 'image/jpeg'});
  String type = image.path.split('.').last;
  String imageUrl = signedUrl.split('.$type').first + '.$type';
  return imageUrl;
}

Future<dynamic> getTickers() async {
  final respose = await Client.getHTTP('/tickers.json');
  return respose.data;
}
